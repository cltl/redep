require('torch')
require('data')
require('util')
require('nn')
require('math')

local _LinearWithoutBias = torch.class('nn.LinearWithoutBias', 'nn.Linear')

    function _LinearWithoutBias:updateOutput(input)
       if input:dim() == 2 then
          local nframe = input:size(1)
          local nElement = self.output:nElement()
          self.output:resize(nframe, self.bias:size(1))
          if self.output:nElement() ~= nElement then
             self.output:zero()
          end
          self.output:addmm(0, self.output, 1, input, self.weight:t())
       else
          error('input must be matrix')
       end
       return self.output
    end

    function _LinearWithoutBias:accGradParameters(input, gradOutput, scale)
       scale = scale or 1
       if input:dim() == 2 then
          self.gradWeight:addmm(scale, gradOutput:t(), input)
       else
          error('input must be a matrix')
       end
    end
    
    function _LinearWithoutBias:parameters()
        return {self.weight}, {self.gradWeight}
    end
    
    
function new_cubenet(label_num, embedding_dims, embedding_num, hidden_sizes, 
        class_num, probabilistic, dropProb)
    local mlp = nn.Sequential()
    local lookup_table = nn.LookupTable(label_num, embedding_dims) 
    mlp:add(lookup_table)
    local dims = embedding_dims*embedding_num
    mlp:add(nn.View(-1, dims))
    if type(hidden_sizes) ~= 'table' then
        hidden_sizes = {hidden_sizes,}
    end
    local hidden_layers = {}
    local lastDims = dims
    for i = 1, #hidden_sizes do
        hidden_layers[i] = nn.Linear(lastDims, hidden_sizes[i]) 
        mlp:add(hidden_layers[i])
        mlp:add(nn.Power(3))
        lastDims = hidden_sizes[i]
    end
    if dropProb and dropProb > 0 then
        mlp:add(nn.Dropout(dropProb))
    end
    local output_layer = nn.LinearWithoutBias(lastDims, class_num)
    mlp:add(output_layer)
    if probabilistic then 
        mlp:add(nn.LogSoftMax())
    end
    mlp.get_lookup_table = function() return lookup_table end
    mlp.get_hidden_layers = function() return hidden_layers end
    mlp.get_output_layer = function() return output_layer end
    return mlp
end

function new_relunet(label_num, embedding_dims, embedding_num, hidden_sizes, 
        class_num, probabilistic, dropProb)
    local mlp = nn.Sequential()
    local lookup_table = nn.LookupTable(label_num, embedding_dims)
    mlp:add(lookup_table)
    local dims = embedding_dims*embedding_num
    mlp:add(nn.View(-1, dims))
    if type(hidden_sizes) ~= 'table' then
        hidden_sizes = {hidden_sizes,}
    end
    local hidden_layers = {}
    local lastDims = dims
    for i = 1, #hidden_sizes do
        hidden_layers[i] = nn.Linear(lastDims, hidden_sizes[i]) 
        mlp:add(hidden_layers[i])
        mlp:add(nn.ReLU(true))
        lastDims = hidden_sizes[i]
    end
    if dropProb and dropProb > 0 then
        mlp:add(nn.Dropout(dropProb))
    end
    local output_layer = nn.LinearWithoutBias(lastDims, class_num)
    mlp:add(output_layer)
    if probabilistic then 
        mlp:add(nn.LogSoftMax())
    end
    mlp.get_lookup_table = function() return lookup_table end
    mlp.get_hidden_layers = function() return hidden_layers end
    mlp.get_output_layer = function() return output_layer end
    return mlp
end

local MaskLayer, Parent = torch.class('nn.MaskLayer', 'nn.Module')

function MaskLayer:__init(masks, filler)
    Parent.__init(self)
    self.filler = filler or 0
    self.masks = masks
    self.batch_masks = torch.ByteTensor() 
end

function MaskLayer:updateOutput(input)
    local data = input[1]
    local states = input[2]
    self.batch_masks:index(self.masks, 1, states)
    self.output:resizeAs(data):fill(self.filler)
    self.output[self.batch_masks] = data[self.batch_masks]
    return self.output
end

function MaskLayer:updateGradInput(input, gradOutput)
    self.gradInput = {gradOutput}
    return self.gradInput
end

function MaskLayer:__tostring__()
  return string.format('%s(%f)', torch.type(self), self.filler)
end

function MaskLayer:type(type_, tensorCache)
    if type_ == 'torch.CudaTensor' then
        self.masks = self.masks:cuda()
        self.batch_masks = self.batch_masks:cuda() 
        self.output = self.output:cuda()
    elseif type_ == 'torch.CudaHalfTensor' then
        self.masks = self.masks:cuda()
        self.batch_masks = self.batch_masks:cuda() 
        self.output = self.output:cudaHalf()
    else
        self.masks = self.masks:byte()
        self.batch_masks = self.batch_masks:byte()
        self.output = self.output:type(type_)
    end
end

function new_masked_net(core, masks, probabilistic)
    local mlp = nn.Sequential()
    local para = nn.ParallelTable()
    para:add(core)
    para:add(nn.Identity())
    mlp:add(para)
    mlp:add(nn.MaskLayer(masks, -math.huge))
    if probabilistic then 
        mlp:add(nn.LogSoftMax())
    end
    mlp.get_lookup_table = function() return core:get_lookup_table() end
    mlp.get_hidden_layers = function() return core:get_hidden_layers() end
    mlp.get_output_layer = function() return core:get_output_layer() end
    return mlp
end

function input_size(x)
    if torch.isTensor(x) then
        return x:size(1)
    elseif torch.type(x) == 'table' then
        return x[1]:size(1)
    else
        error('Unsupported data type: ' .. type(x))
    end
end

function shuffle2(x, y, sx, sy)
    local indices = torch.randperm(input_size(x)):long()
    if torch.isTensor(x) then
        if sx then
            sx:index(x, 1, indices)
        else
            sx = x:index(1, indices)
        end
    elseif torch.type(x) == 'table' then
        if sx then
            for k, v in pairs(x) do
                sx[k]:index(x[k], 1, indices)
            end
        else
            sx = {}
            for k, v in pairs(x) do
                sx[k] = x[k]:index(1, indices)
            end
        end
    else
        error('Unsupported data type: ' .. type(x))
    end
    
    if torch.isTensor(y) then
        if sy then
            sy:index(y, 1, indices)
        else
            sy = y:index(1, indices)
        end
    elseif torch.type(y) == 'table' then
        if sy then
            for k, v in pairs(y) do
                sy[k]:index(y[k], 1, indices)
            end
        else
            sy = {}
            for k, v in pairs(y) do
                sy[k] = y[k]:index(1, indices)
            end
        end
    else
        error('Unsupported data type: ' .. type(y))
    end
    return sx, sy
end

function random_unique_n(x, y, sx, sy, indices, n)
    indices = torch.randperm(indices, input_size(x)):narrow(1, 1, n)
    if torch.isTensor(x) then
        if sx then
            sx:index(x, 1, indices)
        else
            sx = x:index(1, indices)
        end
    elseif torch.type(x) == 'table' then
        if sx then
            for k, v in pairs(x) do
                sx[k]:index(x[k], 1, indices)
            end
        else
            sx = {}
            for k, v in pairs(x) do
                sx[k] = x[k]:index(1, indices)
            end
        end
    else
        error('Unsupported data type: ' .. type(x))
    end
    
    if torch.isTensor(y) then
        if sy then
            sy:index(y, 1, indices)
        else
            sy = y:index(1, indices)
        end
    elseif torch.type(y) == 'table' then
        if sy then
            for k, v in pairs(y) do
                sy[k]:index(y[k], 1, indices)
            end
        else
            sy = {}
            for k, v in pairs(y) do
                sy[k] = y[k]:index(1, indices)
            end
        end
    else
        error('Unsupported data type: ' .. type(y))
    end
    return sx, sy
end

function narrow_all(x, dim, start, size)
    if type(x) == 'table' then
        local ret = {}
        for k = 1, #x do
            ret[k] = x[k]:narrow(1, start, size)
        end
        return ret
    end
    return x:narrow(1, start, size)
end

function random_batch(x, batch_size)
    local batch_start = torch.random(math.max(x:size(1)-batch_size+1, 1))
    local actual_size = math.min(batch_size, x:size(1)-batch_start+1) 
    local batch_x = narrow_all(x, 1, batch_start, actual_size)
    return batch_x 
end

function random_batch2(x, y, batch_size)
    local batch_start = torch.random(math.max(input_size(x)-batch_size+1, 1))
    local actual_size = math.min(batch_size, input_size(x)-batch_start+1) 
    local batch_x = narrow_all(x, 1, batch_start, actual_size)
    local batch_y = narrow_all(y, 1, batch_start, actual_size)
    return batch_x, batch_y 
end

function custom_adagrad(opfunc, x, config, state)
    -- modified from optim.adagrad to match the formula of Stanford implementation
    -- https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/parser/nndep/Classifier.java#L565
   -- (0) get/update state
   if config == nil and state == nil then
      print('no state table, ADAGRAD initializing')
   end
   local config = config or {}
   local state = state or config
   local lr = config.learningRate or 1e-3
   local lrd = config.learningRateDecay or 0
   state.evalCounter = state.evalCounter or 0
   local nevals = state.evalCounter

   -- (1) evaluate f(x) and df/dx
   local fx,dfdx = opfunc(x)

   -- (3) learning rate decay (annealing)
   local clr = lr / (1 + nevals*lrd)
      
   -- (4) parameter update with single or individual learning rates
   if not state.paramVariance then
      state.paramVariance = torch.Tensor():typeAs(x):resizeAs(dfdx):zero()
      state.paramStd = torch.Tensor():typeAs(x):resizeAs(dfdx)
   end
   state.paramVariance:addcmul(1,dfdx,dfdx)
   state.paramStd:resizeAs(state.paramVariance):copy(state.paramVariance):add(config.ada_eps or 1e-6):sqrt()
   x:addcdiv(-clr, dfdx,state.paramStd)

   -- (5) update evaluation counter
   state.evalCounter = state.evalCounter + 1

   -- return x*, f(x) before optimization
   return x,{fx}
end

function train(cfg, train_x, train_y, valid_x, valid_y)
    print(string.format("Training %s...", cfg.model_name))
    local start = os.time()
    local example_count = 0
    local batch_count = math.max(train_y:size(1)/cfg.training_batch_size, 1)
    local best_valid_cost = -math.huge
    local params, grad_params = cfg.mlp:getParameters()
    local params2 = torch.Tensor():typeAs(params) -- avoid allocating multiple times
    local train_sx, train_sy -- shuffled x and y (avoid allocating multiple times)
    local indices = torch.LongTensor()
    local monitoring_cost = function(x, y)
        cfg.mlp:evaluate() -- affects dropout layer, if present
        local cost = 0
        local batch_count = math.max(y:size(1)/cfg.monitoring_batch_size, 1)
        for i = 1, batch_count do
            local batch_start = (i-1)*cfg.monitoring_batch_size + 1
            local actual_batch_size = math.min(cfg.monitoring_batch_size, 
                    y:size(1) - batch_start + 1)
            local batch_x = narrow_all(x, 1, batch_start, actual_batch_size)
            local batch_y = y:narrow(1, batch_start, actual_batch_size)
            local c = cfg.criterion:forward(cfg.mlp:forward(batch_x), batch_y)
            cost = cost + c * actual_batch_size
        end
        cost = cost / y:size(1)
        if cfg.l1_weight and cfg.l1_weight > 0 then
            for _, h in ipairs(cfg.mlp:get_hidden_layers()) do
                cost = cost + torch.abs(h.weight):sum() * cfg.l1_weight 
            end
        end
        if cfg.l2_weight and cfg.l2_weight > 0 then
            cost = cost + params2:cmul(params, params):sum() * cfg.l2_weight / 2 
        end
        return cost
    end 
    local optim_state = {
            learningRate = cfg.learningRate, 
            ada_eps = cfg.ada_eps,
    }
    assert(optim_state.learningRate > 0)
    assert(optim_state.ada_eps > 0)
    for epoch_count = 1, cfg.max_epochs do
        for batch_no = 1, batch_count do
            cfg.mlp:training() -- affects dropout layer, if present
            train_sx, train_sy = random_unique_n(train_x, train_y, 
                                                 train_sx, train_sy, indices, 
                                                 cfg.training_batch_size)
            local batch_x, batch_y = train_sx, train_sy
            local cost = cfg.criterion:forward(cfg.mlp:forward(batch_x), batch_y)
            assert(tostring(cost) ~= 'nan' and not tostring(cost):find('inf'))
            grad_params:zero()
            cfg.mlp:backward(batch_x, cfg.criterion:backward(cfg.mlp.output, batch_y))
            if cfg.l1_weight and cfg.l1_weight > 0 then
                for _, h in ipairs(cfg.mlp:get_hidden_layers()) do
                    h.gradWeight:add(cfg.l1_weight, torch.sign(h.weight))
                end
            end
            if cfg.l2_weight and cfg.l2_weight > 0 then
                grad_params:add(cfg.l2_weight, params)
            end
            if cfg.max_grad and cfg.max_grad > 0 then
                grad_params:clamp(-cfg.max_grad, cfg.max_grad)
            end
            custom_adagrad(function() return nil, grad_params end, params, optim_state)
            
            example_count = example_count + cfg.training_batch_size
            if cfg.batch_reporting_frequency > 0 and
                    batch_no % cfg.batch_reporting_frequency == 0 then
                local speed = example_count / (os.time()-start)
                io.write(string.format("Batch %d, speed = %.2f examples/s\r", batch_no, speed))
            end
        end
        if cfg.batch_reporting_frequency > 0 and
                batch_count >= cfg.batch_reporting_frequency then
            io.write('\n')
        end
        collectgarbage() -- important! avoid memory error

        if cfg.epoch_reporting_frequency > 0 and 
                epoch_count % cfg.epoch_reporting_frequency == 0 then
            print(string.format("Epoch %d:", epoch_count))
            print(string.format("\tTraining cost: %f", 
                    monitoring_cost(train_sx, train_sy)))
            if valid_x ~= nil and valid_y ~= nil then
                print(string.format("\tValid cost: %f", 
                        monitoring_cost(valid_x, valid_y)))
            end
            print(string.format("\tSeconds since start: %d s", os.time()-start))
        end
        
        if valid_x and valid_y and cfg.best_path then
            local valid_cost = monitoring_cost(valid_x, valid_y)
            if best_valid_cost > valid_cost then
                io.write(string.format('Writing best model to %s... ', cfg.best_path)) 
                local light_mlp = cfg.mlp:clone('weight','bias')
                torch.save(cfg.best_path, light_mlp)
                best_valid_cost = valid_cost
                print('Done.')
            end
        end
    end
    if valid_x ~= nil and valid_y ~= nil and cfg.best_path ~= nil then
        io.write(string.format('Loading from %s... ', cfg.best_path))
        cfg.best_mlp = torch.load(cfg.best_path)
        print('Done.')
    end
    local stop = os.time()
    print(string.format("Training %s... Done (%.2f min).", cfg.model_name, (stop-start)/60.0))
end

