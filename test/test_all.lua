require('paths')

paths.dofile('test_data.lua')
paths.dofile('test_model.lua')
paths.dofile('test_util.lua')
paths.dofile('../dep/test/test_feature.lua')
paths.dofile('../dep/test/test_reinforcement.lua')
paths.dofile('../dep/test/test_task.lua')

print('All tests passed.')