require('torch')
require('model')

local types = {{'a', 2}, {'b', 3}, {'c', 5}}
local max_labels = 100
local nClasses = 10


local x = torch.LongTensor(100, 5)    
x:random(max_labels)
local y = torch.LongTensor(100)
y:random(nClasses)

function test_cubenet()
    local m = new_cubenet(max_labels, 50, 5, 32, nClasses, true)
    criterion = nn.ClassNLLCriterion()
    assert(criterion:forward(m:forward(x), y) > 0)
end

function test_random_batch()
    local bx, by = random_batch2(x, y, 10)
    assert(bx:size(1) == 10)
    assert(bx:size(2) == 5)
    assert(by:size(1) == 10)
    local bx, by = random_batch2(x, y, 100)
    assert(bx:size(1) == 100)
    assert(by:size(1) == 100)
end

function test_shuffle2()
    local x2, y2 = shuffle2(x, y)
    assert(x2[1]:size(1) == x[1]:size(1))
    assert(y2:size(1) == y:size(1))
    -- assert(y2[1] ~= y[1])
    local a = torch.rand(10, 5)
    local b = torch.rand(10, 3)
    local a2, b2 = shuffle2(a, b)
    assert(a2:size(1) == a:size(1))
    assert(b2:size(1) == b:size(1))
end

function test_mask()
    local data = torch.Tensor(10, 3)
    data:uniform()
    local states = torch.LongTensor({1,1,2,2,3,1,5,3,2,4})
    local masks = torch.ByteTensor({{1,0,0},{0,1,0},{0,0,1},{1,1,0},{0,0,0}})
    local mlp = nn.MaskLayer(masks)
    local y = mlp:forward{data, states}
    assert(y[1][1] == data[1][1])
    assert(y[1][2] == 0 and y[1][3] == 0)
    assert(y[7]:eq(0):all())
end

test_mask()
test_cubenet()
test_shuffle2()
test_random_batch()
print('OK')
