require('data')
require('util')
require('paths')

local conll = CoNLL()
dir = paths.dirname(paths.thisfile())
sent_path = paths.concat(dir, '1-sent.conll')
ds = conll:build_dataset(sent_path, 'test', 100)
sents_path = paths.concat(dir, '10-sents.conll')

function test_parse_sentence()
    assert(ds.sents:size(1) == 1)
    assert(ds.tokens:size(1) == 19)
    assert(ds.tokens[1][1] == conll.vocabs.word:get_index('__ROOT__'))
    assert(ds.tokens[1][2] == conll.vocabs.pos:get_index('__ROOT__'))
    assert(ds.tokens[1][3] == 0) 
    assert(ds.tokens[2][1] == conll.vocabs.word:get_index('Pierre'))
    assert(ds.tokens[2][2] == conll.vocabs.pos:get_index('NNP'))
    assert(ds.tokens[2][3] == 3) 
    assert(ds.tokens[19][1] == conll.vocabs.word:get_index('.'))
    assert(ds.tokens[19][2] == conll.vocabs.pos:get_index('.'))
    assert(ds.tokens[19][3] == 9) 
end

function test_parse_all_in_file()
    local ds2 = conll:build_dataset(sents_path, 'test', 500)
    assert(ds2.sents:size(1) == 10)
end

function test_vocab()
    vocab = Vocabulary()
    assert(vocab:get_index('__MISSING__') == 1)
    assert(vocab:get_index('a') == 2)
    assert(vocab:get_index('a') == 2)
    assert(vocab:get_index('b') == 3)
    assert(vocab:get_word(1) == '__MISSING__')
    assert(vocab:get_word(2) == 'a')
    assert(vocab:get_word(3) == 'b')
end

function test_max_index()
    local indexer = Indexer()
    local vocabs = {
        a = Vocabulary(indexer), 
        b = Vocabulary(indexer), 
    }
    vocabs.a:get_index('x')
    vocabs.a:get_index('y')
    vocabs.b:get_index('x')
    vocabs.b:get_index('y')
    vocabs.b:get_index('z')
    assert(max_index(vocabs) == 5)
end

function test_prune()
    vocab = Vocabulary()
    assert(vocab:get_index('a') == 1)
    assert(vocab:get_index('a') == 1)
    assert(vocab:get_index('b') == 2)
    vocab:prune(1)
    assert(vocab:size() == 2)
    vocab:prune(2)
    assert(vocab:size() == 1)
end

function test_word2vec_bin()
    vocab, M = read_word2vec_bin('test/vectors-small.bin', false, true)
    assert(vocab:size() == 43)
    assert(#vocab.word2index == 43)
    assert(M:size()[1] == 43)
    assert(M:size()[2] == 200)
    assert(vocab.word2index['</s>'] > 1)
    assert(vocab.word2index['of'] > 1)
    assert(vocab.word2index['Rockwell'] > 1)
    assert(vocab.word2index['CorpPUNT'] > 1)
    assert(vocab.word2index["'s"] > 1)
end

function test_write()
    local tmpname = os.tmpname()
    conll:write_sentence(tmpname, ds.tokens)
    print('Dataset written to ' .. tmpname)
    local content = list(io.lines(tmpname))
    assert(#content == 18)
    
    local ds2 = conll:build_dataset(sents_path, 'test', 500)
    local orig_content = list(io.lines(sents_path))
    local tmpname = os.tmpname()
    conll:write_all_sentences(tmpname, ds2)
    print('Dataset written to ' .. tmpname)
    local content = list(io.lines(tmpname))
    assert(#orig_content == #content or #orig_content == #content-1)
    for i = 1, #orig_content do
        assert(orig_content[i] == content[i])
    end
end

function test_kfold_interleaving()
    local iter_gen = function() return list_iter(list(range(20))) end
    local folds = kfold_interleaving(iter_gen, 10)
    
    local it = folds[1].test()
    assert(it() == 1)
    assert(it() == 11)
    assert(#list(folds[1].test()) == 2)
    assert(folds[1].train()() == 2)
    assert(#list(folds[1].train()) == 18)

    assert(folds[2].test()() == 2)
    assert(#list(folds[2].test()) == 2)
    assert(folds[2].train()() == 1)
    assert(#list(folds[2].train()) == 18)

    assert(folds[10].test()() == 10)
    assert(#list(folds[10].test()) == 2)
    assert(folds[10].train()() == 1)
    assert(#list(folds[2].train()) == 18)
end

function test_kfold_contiguous()
    local iter_gen = function() return list_iter(list(range(20))) end
    local folds = kfold_contiguous(iter_gen, 10, nil, true)
    
    local it = folds[1].test()
    assert(it() == 1)
    assert(it() == 2)
    assert(#list(folds[1].test()) == 2)
    assert(folds[1].valid()() == 3)
    assert(folds[1].train()() == 5)
    assert(#list(folds[1].train()) == 16)

    assert(folds[2].test()() == 3)
    assert(#list(folds[2].test()) == 2)
    assert(folds[2].train()() == 1)
    assert(#list(folds[2].train()) == 16)

    assert(folds[10].test()() == 19)
    assert(#list(folds[10].test()) == 2)
    assert(folds[10].train()() == 1)
    assert(#list(folds[2].train()) == 16)
end

function test_read_vectors_from_text_file()
    local vocab, M = read_vectors_from_text_file('test/vectors-small.txt', false, ' ')
    assert(M:size(1) == 11)
    assert(vocab:size() == 11)
    assert(vocab.word2index['the'] == 2)
    assert(vocab.word2index['are'] == 11)
    assert(M[2][1] == -0.0279698616277)
    assert(M[11][4] == 0.0215192860064)
    assert(vocab.word2index['.'] ~= nil)
    local vocab, M = read_vectors_from_text_file('test/vectors-small.tsv', false, '\t')
    assert(M:size(1) == 11)
    assert(vocab:size() == 11)
    assert(vocab.word2index['the'] == 2)
    assert(vocab.word2index['"'] == 11)
    assert(M[2][1] == -0.060291999999999998)
    assert(M[11][1] == 0.073356000000000005)
    assert(vocab.word2index['.'] ~= nil)
end

function test_projective()
    assert(is_projective(torch.LongTensor{0, 1, 2})) -- simple
    assert(is_projective(torch.LongTensor{0, 1, 2, 2, 7, 7, 4})) -- complex
    assert(not is_projective(torch.LongTensor{0, 1, 1, 2})) -- crossing edges
    assert(not is_projective(torch.LongTensor{0, 3, 2, 1})) -- cyclic
end

function test_left_child()
    local links = torch.LongTensor{
            {0, 0, 0}, 
            {1, 0, 0}, 
            {2, 0, 0}, 
            {1, 0, 0}, 
    }
    assert(cget_left_dependent(links, 1, 1) == nil)
    assert(cget_left_dependent(links, 2, 1) == nil)
    assert(cget_left_dependent(links, 2, 2) == nil)

    local links = torch.LongTensor{
            {3, 0, 0}, 
            {3, 0, 0}, 
            {0, 0, 0}, 
            {3, 0, 0}, 
    }
    assert(cget_left_dependent(links, 3, 1) == 1)
    assert(cget_left_dependent(links, 3, 2) == 2)
    assert(cget_left_dependent(links, 3, 3) == nil)
    
    local links = torch.LongTensor{
            {3, 0, 0, 0, 2}, 
            {3, 0, 0, 0, 4}, 
            {0, 0, 0, 1, 0}, 
            {3, 0, 0, 0, 0}, 
    }
    assert(cget_left_dependent(links, 3, 1) == 1)
    assert(cget_left_dependent(links, 3, 2) == 2)
    assert(cget_left_dependent(links, 3, 3) == nil)
end


function test_right_child()
    local links = torch.LongTensor{ -- having children, all to the right
            {0, 0, 0}, 
            {1, 0, 0}, 
            {2, 0, 0}, 
            {1, 0, 0}, 
    }
    assert(cget_right_dependent(links, 1, 1) == 4)
    assert(cget_right_dependent(links, 1, 2) == 2)
    assert(cget_right_dependent(links, 1, 3) == nil)
    assert(cget_right_dependent(links, 2, 1) == 3)
    assert(cget_right_dependent(links, 2, 2) == nil)
    
    local links = torch.LongTensor{ -- having children but not to the right
            {4, 0, 0}, 
            {3, 0, 0}, 
            {4, 0, 0}, 
            {0, 0, 0}, 
    }
    assert(cget_right_dependent(links, 1, 1) == nil)
    assert(cget_right_dependent(links, 2, 1) == nil)
    assert(cget_right_dependent(links, 3, 1) == nil)
    assert(cget_right_dependent(links, 4, 1) == nil)
    assert(cget_right_dependent(links, 4, 2) == nil)
    
    local links = torch.LongTensor{ -- having children, some left, some right
            {3, 0, 0}, 
            {3, 0, 0}, 
            {4, 0, 0}, 
            {3, 0, 0}, 
    }
    assert(cget_right_dependent(links, 1, 1) == nil)
    assert(cget_right_dependent(links, 2, 1) == nil)
    assert(cget_right_dependent(links, 3, 1) == 4)
    assert(cget_right_dependent(links, 3, 2) == nil)
    assert(cget_right_dependent(links, 4, 1) == nil)
    
    local links = torch.LongTensor{ -- having children, some left, some right, linked list available
            {3, 0, 0, 0, 2}, 
            {3, 0, 0, 0, 4}, 
            {4, 0, 0, 1, 0}, 
            {3, 0, 0, 0, 0}, 
    }
    assert(cget_right_dependent(links, 1, 1) == nil)
    assert(cget_right_dependent(links, 2, 1) == nil)
    assert(cget_right_dependent(links, 3, 1) == 4)
    assert(cget_right_dependent(links, 3, 2) == nil)
    assert(cget_right_dependent(links, 4, 1) == nil)
end

test_left_child()
test_right_child()
test_projective()
test_max_index()
test_parse_sentence()
test_parse_all_in_file()
test_read_vectors_from_text_file()
test_kfold_interleaving()
test_kfold_contiguous()
test_vocab()
test_write()
test_prune()
test_word2vec_bin()
print('OK')