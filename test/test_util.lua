require('util')

function test_list()
    -- empty list
    l = list()
    assert(#l == 0)
    table.insert(l, 1)
    assert(#l == 1)
    -- from another list
    l = list({'a','b','c'})
    assert(#l == 3)
    assert(l[1] == 'a')
    assert(l[2] == 'b')
    assert(l[3] == 'c')
    -- from range
    l = list(range(5, 10))
    assert(#l == 6)
end

function test_set()
    -- empty set
    s = set()
    assert(len(s) == 0)
    s[1] = 0
    assert(len(s) == 1)
    -- from a map
    s = set({a=1,b=2,c=3})
    assert(s['a'] == true)
    assert(s['b'] == true)
    assert(s['c'] == true)
    assert(len(s) == 3)
    -- from a list
    s = set({'a','b','c'})
    assert(#s == 3)
    assert(s[1] == true)
    assert(s[2] == true)
    assert(s[3] == true)
    assert(len(s) == 3)
    -- from range
    s = set(range(5, 10))
    assert(s[5])
    assert(s[10])
    assert(len(s) == 6)
end

function test_range()
    values = {}
    for i in range(5, 10) do
        table.insert(values, i)
    end
    assert(#values == 6)
    assert(values[1] == 5)
    assert(values[#values] == 10)
    f, last, first = range(10)
    assert(f(_, first) == 1)
end

function test_dict()
    a = dict()
    assert(len(a) == 0)
    a = dict({1,2,3})
    assert(len(a) == 3)
end

function test_split()
    assert(len(split('Minh,student,VU,26', ',')) == 4)
    assert(len(split('Minh,student,,VU,26', ',')) == 4)
    assert(len(split(',Minh,student,VU,26', ',')) == 4)
    assert(len(split('Minh,student,VU,26,,', ',')) == 4)
    assert(len(split('', ',')) == 0)
    assert(len(split(',', ',')) == 0)
end 

function test_strip()
    assert(strip('   12  ') == '12')
    assert(strip('\n12') == '12')
    assert(strip('     12') == '12')
    assert(strip('12 \t  ') == '12')
    assert(strip('12') == '12')
end

function test_iter_first_n()
    local list100 = list(range(100))
    assert(iter_size(iter_first_n(list_iter(list100), -1)) == 0)
    assert(iter_size(iter_first_n(list_iter(list100), 0)) == 0)
    assert(iter_size(iter_first_n(list_iter(list100), 10)) == 10)
    assert(iter_size(iter_first_n(list_iter(list100), 100)) == 100)
    assert(iter_size(iter_first_n(list_iter(list100), 101)) == 100)
end

function test_linked_list()
    l = torch.LongTensor(10)
    l:zero()
    head = 0
    head = ll_insert(l, head, 3)
    assert(torch.eq(l, torch.LongTensor{0,0,0,0,0,0,0,0,0,0}))
    head = ll_insert(l, head, 2)
    assert(torch.eq(l, torch.LongTensor{0,3,0,0,0,0,0,0,0,0}))
    head = ll_insert(l, head, 5)
    assert(torch.eq(l, torch.LongTensor{0,3,5,0,0,0,0,0,0,0}))
    head = ll_insert(l, head, 10)
    assert(torch.eq(l, torch.LongTensor{0,3,5,0,10,0,0,0,0,0}))
    head = ll_insert(l, head, 1)
    assert(torch.eq(l, torch.LongTensor{2,3,5,0,10,0,0,0,0,0}))
    assert(ll_get(l, 3, 1) == 3)
    assert(ll_get(l, 3, 2) == 5)
    assert(ll_get(l, 2, 2) == 3)
    assert(ll_get(l, 1, 2) == 2)
    assert(ll_get(l, 1, 3) == 3)
    assert(ll_get(l, 1, 5) == 10)
    assert(ll_get(l, 1, 6) == nil)
    assert(ll_get(l, 1, 7) == nil)
    assert(ll_rget(l, 1, 1) == 10)
    assert(ll_rget(l, 1, 2) == 5)
    assert(ll_rget(l, 1, 3) == 3)
    assert(ll_rget(l, 1, 4) == 2)
end

test_linked_list()
test_iter_first_n()
test_list()
test_set()
test_dict()
test_range()
test_split()
test_strip()
print('OK')