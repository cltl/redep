## Install software

[Install torch7](http://torch.ch/docs/getting-started.html) on your system.

Install dependencies: 

```
luarocks install cutorch
luarocks install cunn
luarocks install tds
```

## Prepare data

1. Download word cluster from [Stanford POS tagger website](http://nlp.stanford.edu/software/pos-tagger-faq.shtml#distsim) and put into folder `stanford-postagger-2015-12-09`
2. Download [PENN Treebank 3](https://catalog.ldc.upenn.edu/LDC99T42) and copy *directories* under `parsed/mrg/wsj` into a directory named `penntree` in project root directory. The directory structure should look like `redep/penntree/00`, `redep/penntree/01`,... DON'T copy `MERGE.LOG`.
3. Run `th dep/prepare.lua` (usually it takes less than 2 hours but if jackknifing is enabled, it takes about a day to finish).

## Run experiments

> Notice: all experiments write to standard output/error. If you want to inspect 
> the results later, make sure you redirect it to some file. Some experiments
> can take days to finish so you might want to use `nohup` or `screen`.

Main experiments:

1. Reinforcement learning experiments: run `th dep/exp_reinforcement.lua`. If you have access to a CUDA-enabled GPU, add `--cuda`. In our system, it takes a little more than 2 days.
2. Accuracy vs. length: *after RL experiments successfully complete*, run `th dep/exp_accuracy_vs_length.lua`. Two plots will be written to `output/dep/las-vs-length.pdf` and `output/dep/uas-vs-length.pdf`. 

Other experiments:

1. Run Stanford parser + published models on standard test sets: `th dep/stanford-published-model.lua`
2. Train and test Stanford parser on standard split of data: `th dep/stanford-training.lua`
3. Run re-implemented parser + published models on standard test sets: `th dep/parse-published-model.lua`
4. Train and test re-implemented parser on standard split of data: `th dep/exp_chen_manning_2014.lua`. (use `--cuda` if possible, it will take about 1.5 hour).
5. Measure statistics of datasets: `th dep/stats.lua`
6. Run all tests (useful for development and maintenance): `th test/test_all.lua`

Some configurations can be altered by changing `dep/config.lua`.

- Turning on NP-bracketing: uncomment line `data_dir = paths.concat(home_dir, '../penntree.np-bracketing')`
- Turning on POS tag jackknifing: uncomment line `jackknife_dir = paths.concat(out_dir, 'penntree.jackknife')`
- Turning on POS tagger's model caching: see the next line, since training POS taggers is the most time consuming preprocessing step, turning this option on can save you some time. 

You will need to clean the output directory by yourself (see `out_dir` in the config file) and rerun `prepare.lua`.

## System requirements

All experiments were carried out in a virtual environment equipped with:

- CPU: Haswell, no TSX
- RAM: 62 GB
- GPUs: GRID K2 (used only 1 of 2 GPUs)

Reference running time was also measured in this environment.
Scripts may or may not run on systems with different specifications.

## References

Chen, D., & Manning, C. (2014). A Fast and Accurate Dependency Parser using Neural Networks. In Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP) (pp. 740–750). Doha, Qatar: Association for Computational Linguistics.