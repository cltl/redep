require('util')

local words_file = io.lines('embeddings/words.lst')
local vectors_file = io.lines('embeddings/embeddings.txt')
local merged_file = io.open('embeddings/merged.txt', 'wt')

for word, vectors in iter_zip(words_file, vectors_file) do 
    -- word = word:gsub("^''(.+)''$", '%1')
    merged_file:write(word)
    merged_file:write(' ')
    merged_file:write(vectors)
    merged_file:write('\n')
end

merged_file:close()
-- words_file:close()
-- vectors_file:close()