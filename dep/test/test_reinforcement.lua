require('paths')
-- require('data')
require('dep.task')
require('dep.reinforcement')

conll_path = 'test/1k-words-sd.conll'

-- data_dir = 'output/dep/test_reinforcement'
-- paths.mkdir(data_dir)
-- print('Data dir: ' .. data_dir)
-- vocab_path = paths.concat(data_dir, 'vocab.th7')
-- action_path = paths.concat(data_dir, 'action.th7')
-- ds_path = paths.concat(data_dir, 'ds.th7')

-- supervised_model_path = paths.concat(data_dir, 'supervised-model.th7')
-- reinforce_model_path = paths.concat(data_dir, 'reinforce-model.th7')
-- supervised_output_path = paths.concat(data_dir, 'supervised-output.conll')
-- reinforce_output_path = paths.concat(data_dir, 'reinforce-output.conll')

local conll = CoNLL(1, false, false)
conll:prepare(conll_path, '1k-words', 1000)
-- torch.save(vocab_path, conll.vocabs)

local rels = {}
for rel, _ in pairs(conll.vocabs.label.word2index) do
    table.insert(rels, rel)
end
local actions = make_actions(rels, conll.vocabs.label)
-- torch.save(action_path, actions)

local ds = conll:build_dataset(conll_path, '1k-words', 1000)
-- torch.save(ds_path, ds)

local cfg = {
    training_batch_size = 100,
    training_subbatch_size = 100,
    max_epochs = 1,
    criterion = NegativeScaleCriterion(),
    l2_weight = 0,
    best_path = paths.tmpname(),
    batch_reporting_frequency = 10,
    feature_templates = ChenManningFeatures(),
    include_punc = false, -- include punctuation in development-set evaluation
    ada_eps = 1e-6,
    sample_size = 10,
    cuda = false,
    baseline = 0,
}
cfg.mlp = new_masked_net(new_cubenet(max_index(conll.vocabs), 20, 
        cfg.feature_templates:num(), 10, max_index(actions.vocab), 
        false, 0.5), ArcStandardTransitionSystem(actions):build_masks(), true)

local agent = Agent(cfg, conll.vocabs, actions)
agent:sample(ds.sents, ds.tokens)

--[[
exec('th dep/train_chen_manning_2014.lua --modelFile %s ' ..
        '--vocab_path %s --action_path %s --train_path %s --valid_path %s ' ..
        '--batchSize 1000 --maxEpoch 2 --l2 0',
        supervised_model_path, vocab_path, action_path, ds_path, ds_path)

exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s --beam 1',
        root_dep_label, supervised_model_path, conll_path, supervised_output_path)

exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
        'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
        conll_path, supervised_output_path)


exec('th dep/train_reinforcement.lua --input_model %s ' ..
        '--output_model %s --train_path %s --valid_path %s --maxEpoch 10',
        supervised_model_path, reinforce_model_path, ds_path, ds_path)

exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s --beam 1',
        root_dep_label, supervised_model_path, conll_path, reinforce_output_path)

exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
        'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
        conll_path, reinforce_output_path)
--]]