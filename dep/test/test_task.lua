require('dep.task')
require('dep.feature')
require('data')

local tokens = torch.LongTensor{
    {31, 9, 0, 0, 0}, -- root
    {29, 2, 3, 3, 3}, -- 1st word
    {7,  3, 1, 3, 3}, -- 2nd word
    {11, 2, 3, 3, 3}, -- 3rd word
    {13, 2, 4, 3, 3}, -- 4th word
}
local ds = {
    sents = torch.LongTensor{
        {1, 1, 5},
    },
    ['tokens'] = tokens
}
local input_vocabs = defaultdict(function() return Vocabulary() end)
local actions = make_actions({'dep', 'prop', 'v', 'root'}, input_vocabs.label)
-- print(actions)

function test_execute()
    local sys = ArcStandardTransitionSystem(actions, tokens)
    sys:execute(actions.shift)
    sys:execute(actions.shift)
    -- print(sys.stack)
    sys:execute(actions.vocab:get_index('L(dep)'))
    -- print(sys.stack)
    sys:execute(actions.vocab:get_index('L(dep)'))
    -- print(sys.stack)
    sys:execute(actions.shift)
    -- print(sys.stack)
    sys:execute(actions.vocab:get_index('R(v)'))
    sys:execute(actions.vocab:get_index('R(v)'))
    -- print(sys.stack)
    -- print(sys.links)
    assert(sys.links[1][1] == 0)
    assert(sys.links[2][1] == 4)
    assert(sys.links[3][1] == 4)
    assert(sys.links[4][1] == 1)
    assert(sys.links[5][1] == 4)
end


function test_shortest_stack_oracle()
    local oracle = ShortestStackArcStandardOracle(input_vocabs, actions, tokens)
    
    action = oracle:next()
    assert(action == actions.shift)
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == actions.vocab:get_index('L(v)'))
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == actions.shift)
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == actions.shift)
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == actions.vocab:get_index('R(v)'))
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == actions.vocab:get_index('R(v)'))
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == actions.vocab:get_index('R(v)'))
    oracle.parser:execute(action)
    
    action = oracle:next()
    assert(action == nil)
end

function test_build_dataset()
    local oracle = ShortestStackArcStandardOracle(input_vocabs, actions)
    local x, y = oracle:build_dataset(ds, ChenManningFeatures(), '', 100)
    assert(#x == 2)
    assert(x[1]:size()[1] == y:size()[1])
    assert(x[2]:size()[1] == y:size()[1])
end

function test_load_stanford_model()
    local path = 'stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz'
    mlp, vocab, actions = load_stanford_model(path)
    print(mlp)
    assert(mlp)
    assert(vocab)
    assert(actions)
end

local dyno = DynamicArcStandardOracle()

function test_right_stack()
    local stack = {1, 2}
    local buffer = {3, 4, 5} 
    rs = dyno:make_right_stack(stack, buffer, tokens:narrow(2, 3, 3))
    assert(shallow_equal(rs, {3}))
    
    local stack = {1, 2}
    local buffer = {3, 4, 5}
    local links = torch.LongTensor{{0}, {3}, {4}, {5}, {1}}
    rs = dyno:make_right_stack(stack, buffer, links)
    assert(shallow_equal(rs, {3, 4, 5}))
end

function test_tree_frag_unlabeled_loss()
    local gold_links = tokens:narrow(2, 3, 3)
    local links = torch.LongTensor{{0}, {1}, {2}, {3}, {4}}
    assert(dyno:tree_frag_unlabeled_loss(0, links, gold_links) == 2)
    assert(dyno:tree_frag_unlabeled_loss(1, links, gold_links) == 2)
    assert(dyno:tree_frag_unlabeled_loss(2, links, gold_links) == 1)
    assert(dyno:tree_frag_unlabeled_loss(3, links, gold_links) == 0)
    assert(dyno:tree_frag_unlabeled_loss(4, links, gold_links) == 0)
    local links = torch.LongTensor{{0}, {3}, {1}, {3}, {4}}
    assert(dyno:tree_frag_unlabeled_loss(0, links, gold_links) == 0)
    assert(dyno:tree_frag_unlabeled_loss(1, links, gold_links) == 0)
    assert(dyno:tree_frag_unlabeled_loss(2, links, gold_links) == 0)
    assert(dyno:tree_frag_unlabeled_loss(3, links, gold_links) == 0)
    assert(dyno:tree_frag_unlabeled_loss(4, links, gold_links) == 0)
end

function test_edge_delta()
    local gold_links = tokens:narrow(2, 3, 3)
    assert(dyno:edge_delta(0, 1, gold_links) == 0)
    assert(dyno:edge_delta(0, 2, gold_links) == 1)
    assert(dyno:edge_delta(0, 3, gold_links) == 1)
    assert(dyno:edge_delta(3, 2, gold_links) == 0)
    assert(dyno:edge_delta(2, 3, gold_links) == 1)
end

function test_config_loss()
    local sys = ArcStandardTransitionSystem(actions, tokens)
    local gold_links = tokens:narrow(2, 3, 3)
    sys:execute(actions.vocab:get_index('R(dep)'))
    assert(dyno:config_loss(sys, gold_links) == 1)
    
    local sys = ArcStandardTransitionSystem(actions, tokens)
    local gold_links = tokens:narrow(2, 3, 3)
    sys:execute(actions.shift)
    sys:execute(actions.shift)
    sys:execute(actions.vocab:get_index('L(dep)'))
    assert(dyno:config_loss(sys, gold_links) == 3)
    
    local sys = ArcStandardTransitionSystem(actions, tokens)
    local gold_links = tokens:narrow(2, 3, 3)
    sys:execute(actions.shift)
    sys:execute(actions.shift)
    sys:execute(actions.shift)
    assert(dyno:config_loss(sys, gold_links) == 0)  
end

test_execute()
test_config_loss()
test_edge_delta()
test_tree_frag_unlabeled_loss()
test_right_stack()
test_load_stanford_model()
test_shortest_stack_oracle()
test_build_dataset()
print("OK")