require('dep.config')
require('dep.task')
require('data')
require('cunn')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Perform an error analysis')
cmd:text('Options:')
cmd:option('--cuda', false, 'use CUDA')
cmd:option('--print', false, 'print out parses in CoNLL format to compare')
cmd:text()
opt = cmd:parse(arg or {})

local features = predefined_feature_templates['chen_manning']
local test_path = paths.concat(sd_dir, 'test.mrg.dep')

local function parse_starting_from(sys, input_vocabs, actions, feature_extractor, mlp, tokens)
    sys = sys:clone()
    local x = torch.LongTensor(1, feature_extractor:num())
    local states = torch.LongTensor(1)
    while sys:state() ~= sys.states.terminal do
        states[1] = sys:state()
        feature_extractor(x[1], tokens, sys.links, 
                sys.stack, sys.buffer, input_vocabs)
        local action_scores = mlp:forward({x, states})
        local max_score, y = action_scores[1]:max(1)
        local action = y[1]
        sys:execute(action)
    end
    return sys.links
end

local function print_example_sentence(tokens, links, propagated_error_index, processing_order, vocabs, out1, out2)
    local gold_links = tokens:narrow(2, 3, 3)
    for i = 2, tokens:size(1) do
        local token = vocabs.word:get_word(tokens[{i, 1}])
        if processing_order then
            token = token .. '#' .. processing_order[i]
        end
        if propagated_error_index[i] then
            token = token .. '[*]'
        end
        local pos = vocabs.pos:get_word(tokens[{i, 2}])
        out1:write(string.format('%d\t%s\t_\t%s\t%s\t_\t%d\t%s\t_\t_\n', i-1, token, pos, pos, 
                gold_links[{i, 1}]-1, vocabs.label:get_word(gold_links[{i, 2}])))
        out2:write(string.format('%d\t%s\t_\t%s\t%s\t_\t%d\t%s\t_\t_\n', i-1, token, pos, pos,
                links[{i, 1}]-1, vocabs.label:get_word(links[{i, 2}])))
    end
    out1:write('\n')
    out2:write('\n')
end

local function parse_and_measure(ds, input_vocabs, actions, feature_extractor, 
        mlp, report_frequency, out1, out2)
    local start_time = os.time()
    
    print('Parsing...')
    local parser = BatchGreedyArcStandardParser(input_vocabs, actions, 
            feature_extractor, mlp, opt.cuda, 1000)
    local output, _ = parser:predict(ds, 512)
    print('Parsing... Done.')

    print('Analyzing...')
    local error_mask = ds.tokens:narrow(2, 3, 3):ne(output):sum(2):gt(0):squeeze()
    local propagated_error = 0
    for s = 1, ds.sents:size(1) do
        -- print('----- ' .. s)
        local start, size = ds.sents[{s, 2}], ds.sents[{s, 3}]
        local my_tokens = ds.tokens:narrow(1, start, size)
        local my_error_mask = error_mask:narrow(1, start, size)
        my_error_mask[1] = 0 -- don't count root node
        local gold_links = my_tokens:narrow(2, 3, 3)
        local predicted_links = output:narrow(1, start, size):narrow(2, 1, 3)
        if my_error_mask:any() and is_projective(my_tokens:select(2, 3)) then
            local propagated_error_index = {}
            local oracle = ShortestStackArcStandardOracle(input_vocabs, actions, my_tokens)
            local action = oracle:next()
            while action ~= nil do
                if actions.left[action] or actions.right[action] or action == actions.right_root then
                    local dependent
                    if actions.left[action] then
                        dependent = oracle.parser.stack[2]
                    else
                        dependent = oracle.parser.stack[1]
                    end
                    if my_error_mask[dependent] ~= 0 then
                        local links = parse_starting_from(oracle.parser, input_vocabs, 
                                actions, feature_extractor, mlp, my_tokens):narrow(2, 1, 3)
                        if links[dependent]:eq(gold_links[dependent]):all() then
                            propagated_error = propagated_error + 1
                            propagated_error_index[dependent] = true
                        end
                    end
                end
                oracle.parser:execute(action)
                action = oracle:next()
            end
            if out1 and out2 then
                print_example_sentence(my_tokens, predicted_links, 
                        propagated_error_index, processing_order, input_vocabs, 
                        out1, out2)
            end
        end -- of if error
        if report_frequency > 0 and s % report_frequency == 0 then
            print(string.format("Sentence %d, speed = %.2f sentences/s", 
                    s, s/(os.time()-start_time)))
        end
    end -- of for
    print('Analyzing... Done.')
    -- do it after removing errors at root nodes
    local total_error = error_mask:sum()
    return propagated_error, total_error
end
        
function analyze(model_path, model_name)
    local mlp, vocabs, actions
    if model_path:find('%.txt$') or model_path:find('%.txt%.gz$') then
        mlp, vocabs, actions = load_stanford_model(model_path)
    else
        mlp, vocabs, actions = table.unpack(torch.load(model_path))
    end
    if opt.cuda then
        mlp:cuda()
    else
        mlp:float()
    end
    local conll = CoNLL()
    conll.vocabs = vocabs
    local max_rows = iter_size(io.lines(test_path))+1
    local ds = conll:build_dataset(test_path, 'test', max_rows)
    local out1, out2
    if opt.print then
        out1 = io.open(paths.concat(out_dir, 'error_analysis_gold_' .. model_name .. '.conll'), 'w')
        out2 = io.open(paths.concat(out_dir, 'error_analysis_predicted_' .. model_name .. '.conll'), 'w')
    end
    local results = table.pack(parse_and_measure(ds, vocabs, actions, 
            features, mlp, 100, out1, out2))
    if opt.print then
        out1:close()
        out2:close()
    end
    print('#propagated_errors\t#total_errors')
    print(table.concat(results, '\t'))
end

-- main function starts here
local model_path = 'stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz'
analyze(model_path, 'supervised')

local model_path = paths.concat(out_dir, 'sd_reinforcement_chen_manning_2014_random.th7')
analyze(model_path, 'memory')

local model_path = paths.concat(out_dir, 'sd_reinforcement_chen_manning_2014_memory.th7')
analyze(model_path, 'memory')