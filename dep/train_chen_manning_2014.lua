require('dep.config')

require('data')
require('dep.task')
require('dep.feature')
require('model')
require('paths')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Train a model following Chen & Manning (2014)')
cmd:text('Example:')
cmd:text('$> th train_chen_manning_2014.lua')
cmd:text('Options:')
cmd:option('--dependency', 'sd', 'the type of dependency, either sd (Stanford) or lth (LTH/CoNLL)')
cmd:option('--learningRate', 0.01, 'learning rate at t=0')
cmd:option('--l2', 0.00000001, 'strength of l2 regularization')
cmd:option('--dropProb', 0.5, 'Dropout probability. For each training example we randomly choose some amount of units to disable in the neural network classifier. This parameter controls the proportion of units "dropped out."')
cmd:option('--model', 'cube', 'activation function (cube or relu)')
cmd:option('--hiddenSize', '200', 'number of hidden units')
cmd:option('--batchSize', 10000, 'number of examples per batch')
cmd:option('--embeddingDims', 50, 'dimensionality of embeddings (same for words, POS and dependency labels)')
cmd:option('--wordEmbeddings', '', 'path to pretrained word embeddings, can be in text or word2vec binary format')
cmd:option('--cuda', false, 'use CUDA')
cmd:option('--maxIters', 20000, 'maximum number of epochs to run')
cmd:option('--initRange', 0.01, 'Bounds of range within which weight matrix elements should be initialized. Each element is drawn from a uniform distribution over the range [-initRange, initRange].')
cmd:option('--modelFile', '', 'path to save model')
cmd:option('--vocab_path', '', 'path to prepared vocabulary file')
cmd:option('--action_path', '', 'path to prepared action file')
cmd:option('--train_path', '', 'path to prepared training dataset')
cmd:option('--valid_path', '', 'path to prepared valid dataset')
cmd:text()
opt = cmd:parse(arg or {})
assert(opt.modelFile ~= '', 'Path to save trained model is required')
assert(opt.vocab_path ~= '' and opt.action_path ~= '' and 
        opt.train_path ~= '' and opt.valid_path ~= '', 
        'vocab_path, action_path, train_path, valid_path are all required')
print('Command line options: ')
print(opt)
local hiddenSizes = split(opt.hiddenSize, ',')
for i = 1, #hiddenSizes do
    hiddenSizes[i] = tonumber(hiddenSizes[i])
end

local cfg = {
    learningRate = opt.learningRate,
    learningRateDecay = 0,
    training_batch_size = opt.batchSize,
    hidden_size = hiddenSizes,
    max_iters = opt.maxIters,
    l2_weight = opt.l2,
    best_path = paths.tmpname(),
    model_name = opt.model,
    criterion = nn.ClassNLLCriterion(),
    monitoring_batch_size = 100000,
    embedding_dims = opt.embeddingDims,
    feature_templates = ChenManningFeatures(),
    include_punc = false, -- include punctuation in development-set evaluation
    ada_eps = 1e-6,
}
print('General config: ')
print(cfg)

local actions = torch.load(opt.action_path)
local input_vocabs = torch.load(opt.vocab_path)
if opt.model == 'cube' or opt.model == 'cubenet' then 
    cfg.mlp = new_cubenet(max_index(input_vocabs), cfg.embedding_dims, 
            cfg.feature_templates:num(), cfg.hidden_size, max_index(actions.vocab), 
            false, opt.dropProb)
elseif opt.model == 'relu' or opt.model == 'relunet' then
    cfg.mlp = new_relunet(max_index(input_vocabs), cfg.embedding_dims, 
            cfg.feature_templates:num(), cfg.hidden_size, max_index(actions.vocab), 
            false, opt.dropProb)
else
    error('Unsupported model: ' .. opt.model)
end
cfg.mlp = new_masked_net(cfg.mlp, ArcStandardTransitionSystem(actions):build_masks(), true)
print('Model: ')
print(tostring(cfg.mlp))

-- this must be done before importing word embeddings
if opt.initRange then
    local params, _ = cfg.mlp:getParameters()
    params:uniform(-opt.initRange, opt.initRange)
end

if opt.wordEmbeddings ~= '' then
    local pretrained_vocab, pretrained_vectors
    if opt.wordEmbeddings:find('%.bin$') then 
        pretrained_vocab, pretrained_vectors = read_word2vec_bin(opt.wordEmbeddings)
    else
        pretrained_vocab, pretrained_vectors = read_vectors_from_text_file(opt.wordEmbeddings)
    end
    local weight = cfg.mlp.get_lookup_table().weight
    local count = 0
    for word, index in pairs(input_vocabs.word.word2index) do
        local pretrained_index = pretrained_vocab.word2index[word:lower()]
        if pretrained_index then
            weight[index]:copy(pretrained_vectors[pretrained_index])
            count = count + 1
        end
    end
    io.write(string.format('Imported %d words (%.2f%%)\n', count, 
            100*count/#input_vocabs.word.word2index))
end

local oracle = ShortestStackArcStandardOracle(input_vocabs, actions)
local train_ds = torch.load(opt.train_path)
-- for debugging
-- train_ds.sents = train_ds.sents:narrow(1, 1, 1000)
local train_x, train_y = oracle:build_dataset(train_ds, cfg.feature_templates, 'train', 2097203)
local valid_ds = torch.load(opt.valid_path)

if opt.cuda then
    io.write("Initializing GPU... ")
    start = os.time()
    require('cunn')
    if rand_seed then 
        cutorch.manualSeed(rand_seed)
    end
    stop = os.time()
    print(string.format("Done (%d s).", stop-start))

    cfg.mlp:cuda()
    cfg.criterion:cuda()
    for k = 1, #train_x do
        train_x[k] = train_x[k]:cuda()
    end
    train_y = train_y:cuda()
end

local parser = BatchGreedyArcStandardParser(input_vocabs, actions, 
        cfg.feature_templates, cfg.mlp, opt.cuda, 0)
train_parser(cfg, parser, train_x, train_y, valid_ds)
torch.save(opt.modelFile, {cfg.best_mlp, input_vocabs, actions})
