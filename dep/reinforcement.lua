require('dep.task')
require('math')
require('util')
require('paths')
require('optim')
tds = require('tds')

local _NegativeScaleCriterion, parent = torch.class('NegativeScaleCriterion', 'nn.Criterion')
    -- Expect log probabilities as input and pairs (action, scale) as target. 
    -- Scales should be prob(d) * (reward(d) - baseline) where d is a dependency
    -- parse consisting of corresponding actions.
    -- The gradient w.r.t input is -scale * log_prob(action) for unit i=action
    -- and 0 for all the rest (hence scatter operation). The cost as in theory 
    -- is negative (variance-reduced) expected reward: sum prob(d) * (reward(d) - baseline)
    -- however it's hard to compute and not necessary so I omitted it. 
    
    function _NegativeScaleCriterion:__init()
       parent.__init(self)
    end
    
    function _NegativeScaleCriterion:updateOutput(input, target)
    end
    
    function _NegativeScaleCriterion:updateGradInput(input, target)
        self.gradInput:resizeAs(input):zero()
        self.gradInput:scatter(2, target[1]:view(-1, 1), -target[2]:view(-1, 1))
        return self.gradInput
    end


local _Sampling, parent = torch.class('Sampling')
    
    function _Sampling:__init(cfg, vocabs, actions)
        self.cfg = cfg
        self.vocabs = vocabs
        self.actions = actions
        self.output_x = torch.LongTensor()
        self.output_y = torch.LongTensor()
        self.output_states = torch.LongTensor()
        self.output_parses = torch.LongTensor()
        self.action_count = 0
        self.parse_count = 0
    end
    
    function _Sampling:init_tensors(sents, tokens)
        local max_rows = tokens:size(1) * 2 * self.cfg.sample_size
        self.output_x:resize(max_rows, self.cfg.feature_templates:num())
        self.output_y:resize(max_rows)
        self.output_states:resize(max_rows)
        self.output_parses:resize(self.cfg.sample_size*self.cfg.training_batch_size, 4)
        self.action_count = 0
        self.parse_count = 0
    end
    
    function _Sampling:convert(reward, executed_actions, tokens)
        -- convert actions into training examples
        local parser = ArcStandardTransitionSystem(self.actions, tokens)
        local start_index = self.action_count + 1
        for j = 3, #executed_actions do
            self.action_count = self.action_count + 1
            self.output_states[self.action_count] = parser:state()
            self.cfg.feature_templates(self.output_x[self.action_count], tokens, 
                    parser.links, parser.stack, parser.buffer, self.vocabs)
            self.output_y[self.action_count] = executed_actions[j]
            parser:execute(executed_actions[j])
        end
        assert(parser:state() == parser.states.terminal, 'Unfinished parse')
        local stop_index = self.action_count
        self.parse_count = self.parse_count + 1
        self.output_parses[self.parse_count] =
                torch.LongTensor{reward, tokens:size(1), start_index, stop_index-start_index+1}                 
    end

    function _Sampling:find_parser(needle, haystack)
        local actions = needle:get_executed_actions() 
        for index, parser in ipairs(haystack) do
            if shallow_equal(parser:get_executed_actions(), actions) then
                return index
            end
        end
        return nil
    end
    
    function _Sampling:get_results()
        self.output_x, self.output_states, self.output_y = table.unpack(
                narrow_all({self.output_x, self.output_states, self.output_y}, 
                        1, 1, self.action_count))
        self.output_parses = self.output_parses:narrow(1, 1, self.parse_count)
        return self.output_x, self.output_states, self.output_y, self.output_parses
    end


local _OracleOnlySampling, parent = torch.class('OracleOnlySampling', 'Sampling')
    
    function _OracleOnlySampling:__init(cfg, vocabs, actions)
        parent.__init(self, cfg, vocabs, actions)
    end
    
    function _OracleOnlySampling:sample(sents, tokens)
        self:init_tensors(sents, tokens)
        for index = 1, sents:size(1) do
            local my_tokens = tokens:narrow(1, sents[{index, 2}], sents[{index, 3}])
            if is_projective(my_tokens:select(2, 3)) then
                local oracle = ShortestStackArcStandardOracle(self.vocabs, 
                        self.actions, my_tokens):run_until_terminated()
                local reward = my_tokens:size(1) - 1
                self:convert(reward, oracle.parser:get_executed_actions(), my_tokens)
            end
        end
        return self:get_results()        
    end


local _RandomSampling, parent = torch.class('RandomSampling', 'Sampling')
    
    function _RandomSampling:__init(cfg, vocabs, actions, include_oracle)
        parent.__init(self, cfg, vocabs, actions)
        self.x = torch.LongTensor()
        self.states = torch.LongTensor()
        self.include_oracle = include_oracle
    end
    
    function _RandomSampling:init_tensors(sents, tokens)
        parent.init_tensors(self, sents, tokens)
        local num_parsers = sents:size(1)*self.cfg.sample_size
        self.x:resize(num_parsers, self.cfg.feature_templates:num())
        self.states:resize(num_parsers)
        self.x:fill(1) -- avoid accessing illegal index
        self.states:fill(1) -- avoid accessing illegal index
    end
    
    function _RandomSampling:sample_without_replacement(tokens, offset)
        local parsers = {ArcStandardTransitionSystem(self.actions, tokens)}
        local parser_probs = torch.Tensor(self.cfg.sample_size):fill(1)
        local sampled_actions = torch.LongTensor(self.cfg.sample_size)
        while true do
            if parsers[1]:state() == parsers[1].states.terminal then
                -- assume they all terminate at the same time
                for i, parser in ipairs(parsers) do
                    assert(parser:state() == parser.states.terminal)
                end
                break
            end
            for i, parser in ipairs(parsers) do
                self.states[offset + i - 1] = parser:state()
                self.cfg.feature_templates(self.x[offset + i - 1], tokens, 
                        parser.links, parser.stack, parser.buffer, self.vocabs)
            end
            coroutine.yield() -- wait for result
            -- self.probs should be filled with results after this point
            local my_probs = self.probs:narrow(1, offset, #parsers)
            for i, parser in ipairs(parsers) do
                assert(parser_probs[i] > 0)
                my_probs[i]:mul(parser_probs[i])
            end
            local my_flat_probs = my_probs:view(-1)
            my_flat_probs.multinomial(sampled_actions, my_flat_probs, self.cfg.sample_size)
            parser_probs:zero()
            local new_parsers = {}
            for i = 1, sampled_actions:size(1) do
                -- if there is less non-zero entries than sample_size then
                -- torch will return the index of all non-zero entries followed by 1's
                if my_flat_probs[sampled_actions[i]] <= 1e-8 then break end
                parser_probs[i] = my_flat_probs[sampled_actions[i]]
                local parent_index = math.floor((sampled_actions[i]-1) / my_probs:size(2)) + 1 
                local action_index = ((sampled_actions[i]-1) % my_probs:size(2)) + 1
                assert(my_flat_probs[sampled_actions[i]] == my_probs[{parent_index, action_index}])
                local new_parser = parsers[parent_index]:clone()
                new_parser:execute(action_index)
                table.insert(new_parsers, new_parser)
            end
            parser_probs:div(parser_probs:sum())
            parsers = new_parsers
            if #parsers == 0 then
                -- TODO sometimes no parse is sampled, why?
                print('WARN: no parse was sampled.')
                break
            end
        end
        return parsers
    end
    
    function _RandomSampling:batch_predict(func, num)
        local workers = coroutine_create_many(func, num)
        while coroutine_resume_all(workers) do
            -- no need for CUDA since (sub)batch size is rather small 
            self.probs = self.cfg.mlp:forward({self.x, self.states})
            -- see https://github.com/torch/torch7/issues/418
            self.probs = self.probs:double()
            self.probs:exp()
        end
    end
    
    function _RandomSampling:sample(sents, tokens)
        self:init_tensors(sents, tokens)
        local function parse_sentence(index)
            local my_tokens = tokens:narrow(1, sents[{index, 2}], sents[{index, 3}])
            if not is_projective(my_tokens:select(2, 3)) then 
                -- io.write('Ignored a non-projective tree.\n')
                return
            end
            local my_offset = (index-1)*self.cfg.sample_size + 1
            local parsers = self:sample_without_replacement(my_tokens, my_offset)
            local oracle = ShortestStackArcStandardOracle(self.vocabs, 
                    self.actions, my_tokens):run_until_terminated()
            if not self:find_parser(oracle.parser, parsers) then
                table.insert(parsers, oracle.parser)
            end
            for i, parser in ipairs(parsers) do
                local reward = measure_las(my_tokens, parser.links, 
                        false, self.vocabs) * (my_tokens:size(1) - 1)
                self:convert(reward, parser:get_executed_actions(), my_tokens)
            end
        end
        self:batch_predict(parse_sentence, sents:size(1))
        return self:get_results()        
    end


local _SamplingWithMemory, parent = torch.class('SamplingWithMemory', 'RandomSampling')
    
    function _SamplingWithMemory:__init(cfg, vocabs, actions, include_oracle)
        parent.__init(self, cfg, vocabs, actions, include_oracle)
        self.memory = tds.Hash()
    end
    
    function _SamplingWithMemory:sample(sents, tokens)
        self:init_tensors(sents, tokens)
        local function parse_sentence(index)
            local my_tokens = tokens:narrow(1, sents[{index, 2}], sents[{index, 3}])
            if not is_projective(my_tokens:select(2, 3)) then 
                -- io.write('Ignored a non-projective tree.\n')
                return
            end
            local my_offset = (index-1)*self.cfg.sample_size + 1
            -- initialize memory
            local my_memory = self.memory[sents[{index, 1}]]
            if not my_memory then
                local oracle = ShortestStackArcStandardOracle(self.vocabs, 
                        self.actions, my_tokens):run_until_terminated()
                my_memory = tds.Hash{
                    rewards = tds.Vec(),
                    actions = tds.Vec(),
                }
                self.memory[sents[{index, 1}]] = my_memory
                if self.include_oracle then
                    local oracle = ShortestStackArcStandardOracle(self.vocabs, 
                            self.actions, my_tokens):oracle_until_terminated()
                    my_memory.rewards:insert(my_tokens:size(1) - 1)
                    my_memory.actions:insert(tds.Vec(oracle.parser:get_executed_actions()))
                end
            end
            -- forget random memories with a given probability
            local mem_count = #my_memory['actions']
            if (self.include_oracle and mem_count > 1) or
                    (not self.include_oracle and mem_count > 0) then
                local forget = torch.Tensor(mem_count):uniform():le(self.cfg.forget_prob)
                for i = mem_count, 2, -1 do
                    if forget[i] == 1 then
                        my_memory['rewards']:remove(i)
                        my_memory['actions']:remove(i)
                    end
                end 
            end
            -- sample new parses
            local parsers = self:sample_without_replacement(my_tokens, my_offset)
            for i, parser in ipairs(parsers) do
                local reward = measure_las(my_tokens, parser.links, 
                        false, self.vocabs) * (my_tokens:size(1) - 1)
                local executed_actions = parser:get_executed_actions()
                
                local min_index = 0
                local min_reward = math.huge
                local duplicated = false
                for t = 1, #my_memory['rewards'] do
                    if my_memory['rewards'][t] < min_reward then
                        min_reward = my_memory['rewards'][t]
                        min_index = t
                    end
                    duplicated = (my_memory['rewards'][t] == reward) and
                            shallow_equal(my_memory['actions'][t], executed_actions)
                    if duplicated then break end
                end
                if not duplicated then
                    if #my_memory['rewards'] < self.cfg.sample_size + 1 then
                        my_memory['rewards']:insert(reward)
                        my_memory['actions']:insert(tds.Vec(executed_actions))
                    elseif reward > min_reward then
                        my_memory['rewards'][min_index] = reward
                        my_memory['actions'][min_index] = tds.Vec(executed_actions)
                    end
                end
            end
            for i = 1, #my_memory['actions'] do
                self:convert(my_memory['rewards'][i], my_memory['actions'][i], my_tokens)                 
            end
        end
        self:batch_predict(parse_sentence, sents:size(1))
        return self:get_results()
    end

function reinforce_parser(cfg, vocabs, actions, train_ds, valid_ds)
    print("Training by reinforcement learning...")
    local start = os.time()
    
    local sentence_count = 0
    local params, grad_params = cfg.mlp:getParameters()
    local light_mlp = cfg.mlp:clone('weight','bias')
    local perms = torch.LongTensor()
    local train_sents = torch.LongTensor()
    local scales = torch.Tensor()
    local sampler
    if cfg.sampling == 'oracle' then 
        sampler = OracleOnlySampling(cfg, vocabs, actions)
    elseif cfg.sampling == 'random' then
        sampler = RandomSampling(cfg, vocabs, actions, true)
    elseif cfg.sampling == 'memory' then
        sampler = SamplingWithMemory(cfg, vocabs, actions, true)
    else
        error('Unsupported sampling method: ' .. cfg.sampling)
    end
    local optim_state = {
    }
    local parser = BatchGreedyArcStandardParser(vocabs, actions, 
            cfg.feature_templates, cfg.mlp, opt.cuda, 0)
    cfg.mlp:evaluate() -- affects dropout layer, if present
--    local best_score = 0  -- for debugging
    local best_score = parse_and_measure_las(valid_ds, parser, cfg.monitoring_batch_size, cfg.include_punc)
    io.write(string.format('LAS on development set before training: %f%%\n', best_score*100))
    local iters_without_improvement = 0
    for iter_count = 1, cfg.max_iters do
--    for iter_count = 1, 1 do  -- for debugging
        local random_indices = torch.randperm(train_ds.sents:size(1)):long()
        train_sents0:index(train_ds.sents, 1, random_indices)
        -- sort by length to improve batch performance
        _, sorted_indices = train_sents0:select(2, 3):sort()
        train_sents:index(train_sents0, 1, sorted_indices)
        local batch_sents = random_batch(train_sents, cfg.training_batch_size)
--        local batch_sents = train_ds.sents:narrow(1, 1, 10)
          
        cfg.mlp:training() -- affects dropout layer, if present
        sentence_count = sentence_count + batch_sents:size(1)
        grad_params:zero() -- will be accumulated via multiple backpropagation
        local sum_scale2 = 0
        for subbatch_start = 1, batch_sents:size(1), cfg.training_subbatch_size do
            local actual_subatch_size = math.min(
                    batch_sents:size(1)-subbatch_start+1, cfg.training_subbatch_size)
            local x, states, actions, parses = sampler:sample(
                    batch_sents:narrow(1, subbatch_start, actual_subatch_size), train_ds.tokens)
            if cfg.cuda then
                x, states, actions = x:cuda(), states:cuda(), actions:cuda()                
            end
            local log_probs = cfg.mlp:forward({x, states})
            local action_log_probs = log_probs:gather(2, actions:view(-1, 1))
            scales:resize(log_probs:size(1))
            for i = 1, parses:size(1) do
                local reward, token_count, action_start, action_count = 
                        parses[{i,1}], parses[{i,2}], parses[{i, 3}], parses[{i, 4}]
                local parse_prob = math.exp(action_log_probs:narrow(1, action_start, action_count):sum())
                local scale = (reward - cfg.baseline * (token_count-1)) * parse_prob / cfg.training_batch_size
                scales:narrow(1, action_start, action_count):fill(scale) 
                -- print(batch_sents[i])
                -- print(scale)
            end
            scales2 = scales:clone():cmul(scales)
            sum_scale2 = sum_scale2 + scales2:sum() 
            local the_scales = scales
            if cfg.cuda then the_scales = scales:cuda() end
            cfg.mlp:backward({x, states}, cfg.criterion:backward(cfg.mlp.output, {actions, the_scales}))
        end
        print("scale: " .. torch.sqrt(sum_scale2))
        if cfg.l2_weight and cfg.l2_weight > 0 then
            grad_params:add(cfg.l2_weight, params)
        end
        print('E: ' .. cfg.mlp:get_lookup_table().weight:norm())
        print('W1: ' .. cfg.mlp:get_hidden_layers()[1].weight:norm())
        print('b1: ' .. cfg.mlp:get_hidden_layers()[1].bias:norm())
        print('W2: ' .. cfg.mlp:get_output_layer().weight:norm())
        print('gradE: ' .. cfg.mlp:get_lookup_table().gradWeight:norm())
        print('gradW1: ' .. cfg.mlp:get_hidden_layers()[1].gradWeight:norm())
        print('gradb1: ' .. cfg.mlp:get_hidden_layers()[1].gradBias:norm())
        print('gradW2: ' .. cfg.mlp:get_output_layer().gradWeight:norm())
        custom_adagrad(function() return nil, grad_params end, params, optim_state)
        
        if cfg.reporting_frequency > 0 and iter_count % cfg.reporting_frequency == 0 then
            local speed = sentence_count / (os.time() - start)
            io.write(string.format("Iter %d, speed = %.2f sentences/s\n", iter_count, speed))
        end
        collectgarbage() -- important! avoid memory error

        cfg.mlp:evaluate() -- affects dropout layer, if present
        local score = parse_and_measure_las(valid_ds, parser, cfg.monitoring_batch_size, cfg.include_punc)
        io.write(string.format('LAS on development set: %f%%\n', score*100))
        if score > best_score then
            io.write(string.format('Writing best model to %s... ', cfg.best_path)) 
            torch.save(cfg.best_path, light_mlp)
            best_score = score
            print('Done.')
            iters_without_improvement = 0
        else
            iters_without_improvement = iters_without_improvement + 1
        end
        if cfg.max_iters_without_improvement > 0 and
                iters_without_improvement > cfg.max_iters_without_improvement then
            break
        end
        
        if iter_count % cfg.batch_reporting_frequency == 0 then
            io.write(string.format("Iteration %d finished (%.2f mins, speed = %.2f sentences/s).\n", 
                    iter_count, (os.time()-start) / 60, sentence_count / (os.time()-start)))
        end
    end
    
    if paths.filep(cfg.best_path) then
        io.write(string.format('Loading from %s... ', cfg.best_path))
        cfg.best_mlp = torch.load(cfg.best_path)
        print('Done.')
    else
        print('No model was saved. No improvement perhaps.')
    end
    print(string.format("Training by reinforcement learning... Done (%.2f min).", (os.time()-start)/60.0))
end
