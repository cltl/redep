require('dep.config')
require('dep.task')
require('data')
require('cunn')

local test_conll_path = paths.concat(sd_dir, 'test.mrg.dep')
local max_rows = iter_size(io.lines(test_conll_path))+1
local features = predefined_feature_templates['chen_manning']

local _StepByStepParser = torch.class('StepByStepParser')

    function _StepByStepParser:__init(input_vocabs, actions, feature_extractor, mlp, tokens)
        -- the mlp must be masked, so that it returns log probability
        -- for valid actions and -inf for the rest
        self.input_vocabs = input_vocabs
        self.actions = actions
        self.feature_extractor = feature_extractor
        self.mlp = mlp
        self.transition_system = ArcStandardTransitionSystem(actions, tokens)
        self.x = torch.LongTensor(1, self.feature_extractor:num())
        self.states = torch.LongTensor(1)
        self.tokens = tokens
    end

    function _StepByStepParser:next()
        local sys = self.transition_system
        assert(sys:state() ~= sys.states.terminal)
        self.states[1] = sys:state()
        self.feature_extractor(self.x[1], self.tokens, sys.links, 
                sys.stack, sys.buffer, self.input_vocabs)
        local action_scores = self.mlp:forward({self.x, self.states})
        local max_score, y = action_scores[1]:max(1)
        local action = y[1]
        sys:execute(action)
        return action
    end
    
    function _StepByStepParser:has_next()
        return self.transition_system:state() ~= self.transition_system.states.terminal
    end


local function is_structurally_different(a, b, actions)
    return (actions.left[a] and not actions.left[b]) or
            (actions.right[a] and not actions.right[b]) or
            (a == actions.shift and b ~= actions.shift)
end

local function count_correct(pred_links, gold_tokens, input_vocabs)
    local mask = gold_tokens:select(2, 3):ne(0) -- not root
    -- match Stanford implementation here:
    -- https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/parser/nndep/ParsingSystem.java#L130
    for _, p in ipairs{"``", "''", ".", ",", ":"} do
        local index = input_vocabs.pos:get_index(p)
        mask:cmul(gold_tokens:select(2, 2):ne(index))
    end
    local gold_heads = gold_tokens:select(2, 3)[mask]
    local gold_labels = gold_tokens:select(2, 4)[mask]
    local pred_heads = pred_links:select(2, 1)[mask]
    local pred_labels = pred_links:select(2, 2)[mask]
    return pred_heads:eq(gold_heads):cmul(pred_labels:eq(gold_labels)):sum(), gold_heads:size(1)
end

local function parse_and_measure(ds, input_vocabs, actions, feature_extractor, mlp, report_frequency)
    report_frequency = report_frequency or 100
    local start_time = os.time()
    local output = torch.LongTensor(ds.tokens:size(1), 3):zero()
    local actions_before_first_error = torch.LongTensor(ds.sents:size(1)):fill(-1)
    local total_correct_before_first_error = 0
    local total_correct_after_first_error = 0
    local total_after_first_error = 0
    local dyn_oracle = DynamicArcStandardOracle()
    local completely_correct_sentences = 0
    local completely_correct_tokens = 0
    for s = 1, ds.sents:size(1) do
        -- print('----- ' .. s)
        local start, size = ds.sents[{s, 2}], ds.sents[{s, 3}]
        local my_tokens = ds.tokens:narrow(1, start, size)
        local gold_links = my_tokens:narrow(2, 3, 3)
        if is_projective(my_tokens:select(2, 3)) then
            local loss = 0
            local parser = StepByStepParser(input_vocabs, actions, feature_extractor, mlp, my_tokens)
            while parser:has_next() and loss == 0 do
                parser:next()
                actions_before_first_error[s] = actions_before_first_error[s] + 1
                loss = dyn_oracle:config_labeled_loss(parser.transition_system, gold_links)
            end
            -- print(actions_before_first_error[s])
            local correct_before_first_error = count_correct(
                    parser.transition_system.links, my_tokens, input_vocabs)
            total_correct_before_first_error = 
                    total_correct_before_first_error + correct_before_first_error
            if loss > 0 then
                while parser:has_next() do
                    parser:next()
                end
                local correct = count_correct(parser.transition_system.links, my_tokens, input_vocabs)
                local correct_after_first_error = correct - correct_before_first_error
                local num_after_first_error = my_tokens:size(1) - 1 - correct_before_first_error
                total_correct_after_first_error = 
                        total_correct_after_first_error + correct_after_first_error
                total_after_first_error = 
                        total_after_first_error + num_after_first_error 
            else
                completely_correct_sentences = completely_correct_sentences + 1
                completely_correct_tokens = completely_correct_tokens + (my_tokens:size(1) - 1)
            end
            
            local sys = parser.transition_system
            assert(#sys.stack == 1 and #sys.buffer == 0 and sys.stack[1] == 1, 'Unfinished parse.')
            output:narrow(1, start, size):copy(sys.links:narrow(2, 1, 3))
        end

        if report_frequency > 0 and s % report_frequency == 0 then
            print(string.format("Sentence %d, speed = %.2f sentences/s", 
                    s, s/(os.time()-start_time)))
        end
    end
    local total_correct, total_tokens = count_correct(output, ds.tokens, input_vocabs)
    assert(total_correct_before_first_error + total_correct_after_first_error == total_correct)
    return {total_correct_before_first_error, total_correct_after_first_error,
            total_tokens, total_after_first_error,
            completely_correct_sentences, completely_correct_tokens,
            actions_before_first_error[actions_before_first_error:ge(0)]:sum()},
            {actions_before_first_error}
end
        
function analyze(model_path)
    local mlp, vocabs, actions
    if model_path:find('%.txt$') or model_path:find('%.txt%.gz$') then
        mlp, vocabs, actions = load_stanford_model(model_path)
    else
        mlp, vocabs, actions = table.unpack(torch.load(model_path))
        mlp:float()
    end
    local conll = CoNLL()
    conll.vocabs = vocabs
    local ds = conll:build_dataset(test_conll_path, 'test', max_rows)
    local individual_measurements, comparative_measurements = 
            parse_and_measure(ds, vocabs, actions, features, mlp, 500)
    print('#correct_before_first_error' ..
            '\t#correct_after_first_error\t#tokens\t#after_first_error' ..
            '\t#completely_correct_sentences\t#completely_correct_tokens' ..
            '\t#actions_before_first_error')
    print(table.concat(individual_measurements, '\t'))
    return comparative_measurements
end

-- main function starts here
local model_path = 'stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz'
local actions_before_first_error1 = table.unpack(analyze(model_path))

local model_path = paths.concat(out_dir, 'sd_reinforcement_chen_manning_2014_memory.th7')
local actions_before_first_error2 = table.unpack(analyze(model_path))

local diff = actions_before_first_error2 - actions_before_first_error1
diff = diff[diff:ne(-1)]
local later, same, before = diff:gt(0):sum(), diff:eq(0):sum(), diff:lt(0):sum()
local later_delta, before_delta = diff[diff:gt(0)]:sum(), -diff[diff:lt(0)]:sum()
print('Comparing RL-Memory to SL in when the first error occur in sentences:')
print('\tLater: ' .. later .. ' (+' .. later_delta .. ')')
print('\tSame: ' .. same)
print('\tBefore: ' .. before .. ' (-' .. before_delta .. ')')
