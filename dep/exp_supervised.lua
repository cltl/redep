require('dep.config')
require('paths')
require('util')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Train and measure performance of supervised models')
cmd:text('Example:')
cmd:text('$> th exp_supervised.lua')
cmd:text('Options:')
cmd:option('--cuda', false, 'use CUDA')
cmd:text()
opt = cmd:parse(arg or {})

local root_dep_label = 'root'
local vocab_path = sd_vocab_path
local action_path = sd_action_path
local train_path = sd_train_path 
local valid_path = sd_valid_path
local valid_conll_path = paths.concat(sd_dir, 'valid.mrg.dep') 
local test_conll_path = paths.concat(sd_dir, 'test.mrg.dep') 
local cuda_flag = ternary(opt.cuda, '--cuda', '')
        
local function run(model_type, hidden_sizes, drop_prob, model_path, valid_output_path, test_output_path)
    exec('th dep/train_chen_manning_2014.lua --maxIters 20000 --learningRate 0.01 ' .. 
            '--model %s --hiddenSize %s --dropProb %f --modelFile %s ' ..
            '--vocab_path %s --action_path %s --train_path %s --valid_path %s %s',
            model_type, hidden_sizes, drop_prob, model_path, 
            vocab_path, action_path, train_path, valid_path, cuda_flag)
            
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            root_dep_label, model_path, valid_conll_path, valid_output_path, cuda_flag)
    
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            valid_conll_path, valid_output_path)
    
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            root_dep_label, model_path, test_conll_path, test_output_path, cuda_flag)
    
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            test_conll_path, test_output_path)
end

local model_path = paths.concat(out_dir, 'sd_supervised_cube.th7')
local valid_output_path = paths.concat(out_dir, 'sd_valid_cube.conll') 
local test_output_path = paths.concat(out_dir, 'sd_test_cube.conll') 
run('cube', '200', 0.5, model_path, valid_output_path, sd_test_output_path)

local model_path = paths.concat(out_dir, 'sd_supervised_relu.th7')
local valid_output_path = paths.concat(out_dir, 'sd_valid_relu.conll') 
local test_output_path = paths.concat(out_dir, 'sd_test_relu.conll') 
-- run('relu', '200,200', 0, model_path, valid_output_path, sd_test_output_path)
