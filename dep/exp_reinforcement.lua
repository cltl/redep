require('dep.config')
require('util')
require('data')
require('dep.task')
require('dep.reinforcement')
require('dep.feature')
require('model')
require('paths')

exec('date')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Run experiments on using reinforcement learning to improve models.')
cmd:text('Example:')
cmd:text('$> th exp_reinforcement.lua --cuda')
cmd:text('Options:')
cmd:option('--cuda', false, 'use CUDA')
cmd:text()
opt = cmd:parse(arg or {})
local cuda_flag = ternary(opt.cuda, '--cuda', '')

local datasets = {
    sd = {
        root_label = 'root',
        train_path = paths.concat(sd_dir, 'train.mrg.dep'), 
        valid_path = paths.concat(sd_dir, 'valid.mrg.dep'), 
        test_path = paths.concat(sd_dir, 'test.mrg.dep')
    },
    lth = {
        root_label = 'ROOT',
        train_path = paths.concat(lth_dir, 'train.mrg.dep'), 
        valid_path = paths.concat(lth_dir, 'valid.mrg.dep'), 
        test_path = paths.concat(lth_dir, 'test.mrg.dep')
    }
}

local function evaluate_supervised(model_path, dep_type)
    io.write('*************************************************************************\n')
    io.write(string.format('Experiment started (supervised, dependency: %s)\n', dep_type))
    io.write('*************************************************************************\n')
    
    local ds = datasets[dep_type]
    
    local output_path = paths.concat(out_dir, 'supervised-valid-' .. dep_type .. '.conll')
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            ds.root_label, model_path, ds.valid_path, output_path, cuda_flag)
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            ds.valid_path, output_path)

    local output_path = paths.concat(out_dir, 'supervised-test-' .. dep_type .. '.conll')
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            ds.root_label, model_path, ds.test_path, output_path, cuda_flag)
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            ds.test_path, output_path)
            
    io.write(string.format('Experiment finished (supervised, dependency: %s)\n\n\n', dep_type))
end

local function run_sampling_method(method, input_model, output_model, dep_type)
    io.write('*************************************************************************\n')
    io.write(string.format('Experiment started (sampling method: %s, input model: %s, dependency: %s)\n', 
            method, input_model, dep_type))
    io.write('*************************************************************************\n')

    local ds = datasets[dep_type]

    local forget_opt = ''
    if method == 'memory' and dep_type == 'sd' then
        forget_opt = '--forget_prob 0.2' 
    end
    exec('th dep/train_reinforcement.lua --rootLabel %s --sampling %s --cuda_model ' ..
            '--input_model %s --output_model %s --train_path %s --valid_path %s ' ..
            '--max_iters_without_improvement 0 %s %s', 
            ds.root_label, method, input_model, output_model, ds.train_path, 
            ds.valid_path, cuda_flag, forget_opt)

    local output_path = paths.concat(out_dir, 'reinforcement-valid-' .. method .. '-' .. dep_type .. '.conll')
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            ds.root_label, output_model, ds.valid_path, output_path, cuda_flag)
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            ds.valid_path, output_path)
    
    local output_path = paths.concat(out_dir, 'reinforcement-test-' .. method .. '-' .. dep_type .. '.conll')
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            ds.root_label, output_model, ds.test_path, output_path, cuda_flag)
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            ds.test_path, output_path)
            
    io.write(string.format('Experiment finished (sampling method: %s, input model: %s, ' ..
            'dependency: %s)\n\n\n', method, input_model, dep_type))
end

local function run_model(input_model, output_model_pattern, dep_type)
--    evaluate_supervised(input_model, dep_type)    
--    local methods = {'oracle', 'random', 'memory'}
    local methods = {'memory'}
    for _, method in ipairs(methods) do
        output_model = string.format(output_model_pattern, method) 
        run_sampling_method(method, input_model, output_model, dep_type)
    end 
end

local output_model_pattern = paths.concat(out_dir, 'sd_reinforcement_%s.th7')
local output_path_pattern = paths.concat(out_dir, 'sd_reinforcement_%s.conll')
-- run_model('', output_model_pattern, output_path_pattern)

local input_model = 'stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz'
local output_model_pattern = paths.concat(out_dir, 'sd_reinforcement_chen_manning_2014_%s.th7')
local output_path_pattern = paths.concat(out_dir, 'sd_reinforcement_chen_manning_2014_%s.conll')
run_model(input_model, output_model_pattern, 'sd')

local input_model = 'stanford-parser-full-2014-10-31/PTB_CoNLL_params.txt.gz'
local output_model_pattern = paths.concat(out_dir, 'lth_reinforcement_chen_manning_2014_%s.th7')
local output_path_pattern = paths.concat(out_dir, 'lth_reinforcement_chen_manning_2014_%s.conll')
-- run_model(input_model, output_model_pattern, 'lth')
