require('dep.config')
require('paths')
require('util')

exec('date')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Run a reimplementation of Chen & Manning (2014) experiment')
cmd:text('Example:')
cmd:text('$> th exp_scaled_supervised.lua')
cmd:text('Options:')
cmd:option('--cuda', false, 'use CUDA')
cmd:text()
opt = cmd:parse(arg or {})

local function run(root_dep_label, model_path, vocab_path, action_path, 
        train_path, valid_path, test_conll_path, test_output_path)
    local cuda_flag = ternary(opt.cuda, '--cuda', '')
        
    exec('th dep/train_scaled_supervised.lua --wordEmbeddings %s --modelFile %s ' ..
            '--vocab_path %s --action_path %s --train_path %s --valid_path %s %s',
            embeddings_path, model_path, vocab_path, action_path, 
            train_path, valid_path, cuda_flag)
        
    exec('th dep/parse.lua --rootLabel %s --modelPath %s --input %s --output %s %s',
            root_dep_label, model_path, test_conll_path, test_output_path, cuda_flag)
    
    exec('java -cp stanford-parser-full-2014-10-31/stanford-parser.jar ' ..
            'edu.stanford.nlp.trees.DependencyScoring -nopunc -conllx True -g %s -s %s',
            test_conll_path, test_output_path)
end

local sd_model_path = paths.concat(out_dir, 'sd_scaled_supervised.th7')
local sd_test_conll_path = paths.concat(sd_dir, 'test.mrg.dep') 
local sd_test_output_path = paths.concat(out_dir, 'sd_scaled_supervised.conll') 
run('root', sd_model_path, sd_vocab_path, sd_action_path, 
        sd_train_path, sd_valid_path, sd_test_conll_path, sd_test_output_path)

local lth_model_path = paths.concat(out_dir, 'lth_scaled_supervised.th7')
local lth_test_conll_path = paths.concat(lth_dir, 'test.mrg.dep') 
local lth_test_output_path = paths.concat(out_dir, 'lth_scaled_supervised.conll') 
run('ROOT', lth_model_path, lth_vocab_path, lth_action_path, 
        lth_train_path, lth_valid_path, lth_test_conll_path, lth_test_output_path)
