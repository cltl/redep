require('dep.config')
require('dep.task')
require('util')
require('gnuplot')

exec('date')

local test_conll_path = paths.concat(sd_dir, 'test.mrg.dep')
local feature_templates = predefined_feature_templates['chen_manning']

function filter_by_length(ds, min, max)
    local lengths = ds.sents:select(2, 3)
    local sent_indices = lengths:ge(min):cmul(lengths:le(max)):nonzero()
    assert(sent_indices:dim() > 0 and sent_indices:size(1) >= 25)
    local sents_by_length = ds.sents:index(1, sent_indices:view(-1))
    local my_sents = torch.LongTensor():resizeAs(sents_by_length)
    local my_tokens = torch.LongTensor(sents_by_length:select(2, 3):sum(), ds.tokens:size(2))
    local sent_count = 0
    local token_count = 0
    for i = 1, sents_by_length:size(1) do
        sent_count = sent_count + 1
        local l = sents_by_length[{i, 3}]
        my_sents[i] = torch.LongTensor{sent_count, token_count + 1, l}
        my_tokens:narrow(1, token_count+1, l):copy(ds.tokens:narrow(1, 
                sents_by_length[{i, 2}], l))
        token_count = token_count + l
    end
    assert(token_count == my_tokens:size(1))
    return {sents=my_sents, tokens=my_tokens}
end

function analyze(model_path)
    local mlp, vocabs, actions
    if model_path:find('%.txt$') or model_path:find('%.txt%.gz$') then
        mlp, vocabs, actions = load_stanford_model(model_path)
    else
        require('cunn')
        mlp, vocabs, actions = table.unpack(torch.load(model_path))
    end

    local conll = CoNLL()
    conll.vocabs = vocabs
    local max_rows = iter_size(io.lines(test_conll_path))+1
    local ds = conll:build_dataset(test_conll_path, test_conll_path, max_rows)
    local bin_size = 10
    local ds_by_length = {}
    local lengths = {}
    for l = 10, 60, 10 do
        table.insert(lengths, l)
        ds_by_length[l] = filter_by_length(ds, l-10+1, l)
    end
    lengths = torch.LongTensor(lengths)

    local parser = BatchGreedyArcStandardParser(vocabs, actions, 
            feature_templates, mlp, false, 0)
    local uas = torch.Tensor(lengths:size(1))
    local las = torch.Tensor(lengths:size(1))
    for i = 1, lengths:size(1) do
        local l = lengths[i]
        io.write(string.format('Measuring accuracy (length bin=%d)... ', l))
        uas[i] = parse_and_measure_uas(ds_by_length[l], parser)
        las[i] = parse_and_measure_las(ds_by_length[l], parser)
        io.write(string.format('Done (%s sentences).\n', ds_by_length[l].sents:size(1)))
    end
    return lengths, uas, las
end

supervised_model_path = 'stanford-parser-full-2014-10-31/PTB_Stanford_params.txt.gz'
supervised_lengths, supervised_uas, supervised_las = analyze(supervised_model_path)

reinforced_model_path = paths.concat(out_dir, 'sd_reinforcement_chen_manning_2014_memory.th7')
reinforced_lengths, reinforced_uas, reinforced_las = analyze(reinforced_model_path)

assert(supervised_lengths:eq(reinforced_lengths):all())
local lengths = reinforced_lengths

gnuplot.pdffigure(paths.concat(out_dir, 'uas-vs-length.pdf'))
gnuplot.plot({'RL-memory', lengths, reinforced_uas, '-'},
        {'supervised', lengths, supervised_uas, '-'})
gnuplot.xlabel('Length')
gnuplot.ylabel('UAS')
gnuplot.plotflush()

gnuplot.pdffigure(paths.concat(out_dir, 'las-vs-length.pdf'))
gnuplot.plot({'RL-memory', lengths, reinforced_las, '-'},
        {'supervised', lengths, supervised_las, '-'})
gnuplot.xlabel('Length')
gnuplot.ylabel('LAS')
gnuplot.plotflush()