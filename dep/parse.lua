require('nn')
require('torch')
torch.setdefaulttensortype('torch.FloatTensor')

require('data')
require('dep.task')
require('dep.config')
require('model')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Parse a CoNLL file')
cmd:text('Example:')
cmd:text('$> th parse.lua --id exp1 --learningRate 0.001 --max_epochs 100')
cmd:text('Options:')
cmd:option('--input', '', 'path to input file')
cmd:option('--output', '', 'path to output file')
cmd:option('--featureType', 'chen_manning', 'type of features (left or left-right)')
cmd:option('--modelPath', '', 'path to th7 file storing a neural network')
cmd:option('--batchSize', 512, 'number of sentences being parsed concurrently')
cmd:option('--beam', 1, 'number of parses to explore concurrently')
cmd:option('--cuda', false, 'use CUDA')
cmd:option('--cuda_model', false, 'signal that the model was save with CUDA tensors')
cmd:option('--rootLabel', 'root', 'a string for root label, different when using LTH or Stanford dependencies')
cmd:text()
opt = cmd:parse(arg or {})
assert(opt.modelPath ~= '', 'modelPath is mandatory')
root_dep_label = opt.rootLabel

assert(opt.input ~= '', 'input is mandatory')
assert(opt.output ~= '', 'ouput is mandatory')
local feature_templates = predefined_feature_templates[opt.featureType]
assert(feature_templates ~= nil, 'Unsupported feature type: ' .. opt.featureType)

local mlp, vocabs, actions
if opt.modelPath:find('%.txt$') or opt.modelPath:find('%.txt%.gz$') then
    mlp, vocabs, actions = load_stanford_model(opt.modelPath)
else
    if opt.cuda_model or opt.cuda then
        require('cunn')
    end
    mlp, vocabs, actions = table.unpack(torch.load(opt.modelPath))
end
if opt.cuda then
    require('cunn')
    if rand_seed then
        cutorch.manualSeed(rand_seed)
    end
    mlp:cuda()
end
mlp:evaluate() -- affects dropout layer, if present
print(mlp)

local conll = CoNLL()
conll.vocabs = vocabs
local max_rows = iter_size(io.lines(opt.input))+1
local ds = conll:build_dataset(opt.input, opt.input, max_rows)
-- for debugging
-- ds.sents = ds.sents:narrow(1, 1, 100)
ds.tokens:narrow(2, 3, 3):zero() -- erase gold dependency tree
local parser
if opt.beam == 1 then
    parser = BatchGreedyArcStandardParser(vocabs, actions, 
            feature_templates, mlp, opt.cuda, 1000)
else
    parser = BatchBeamArcStandardParser(vocabs, actions, 
            feature_templates, mlp, opt.beam, opt.cuda, 100)
end

print('Parsing...') 
local start_time = os.time()
local output, parse_scores = parser:predict(ds, opt.batchSize)
ds.tokens:narrow(2, 3, 3):copy(output)
print(string.format('Parsing... Done (%.2f minutes).', (os.time()-start_time)/60))
parse_scores:exp()
print(string.format('Confidence (per sentence): %.2f%% (std: %.2f%%)',
        parse_scores:mean()*100, parse_scores:std()*100))

conll:substitue_dependency(opt.input, opt.output, ds)
print("Parsing results written to " .. opt.output)