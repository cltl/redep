require('dep.config')

require('data')
require('dep.task')
require('dep.reinforcement')
require('dep.feature')
require('model')

cmd = torch.CmdLine()
cmd:text()
cmd:text('Train using reinforcement learning given a pre-trained model')
cmd:text('Example:')
cmd:text('$> th exp_reinforcement.lua --cuda')
cmd:text('Options:')
cmd:option('--learningRate', 0.01, 'learning rate at t=0')
cmd:option('--l2', 0.00000001, 'strength of l2 regularization')
cmd:option('--batchSize', 512, 'number of sentences per batch')
cmd:option('--subbatchSize', 32, 'batch is divided further to fit in memory')
cmd:option('--sampleSize', 8, 'number of alternatives to explore (0 to disable)')
cmd:option('--cuda', false, 'use CUDA')
cmd:option('--cuda_model', false, 'signal that the model was save with CUDA tensors')
cmd:option('--maxIters', 5000, 'maximum number of epochs to run')
cmd:option('--max_iters_without_improvement', 200, 'early stopping')
cmd:option('--sampling', 'memory', 'method of sampling')
cmd:option('--input_model', '', 'path to an input model')
cmd:option('--output_model', '', 'path to an output model')
cmd:option('--train_path', '', 'path to compiled training dataset')
cmd:option('--valid_path', '', 'path to compiled development dataset')
cmd:option('--forget_prob', 0.05, 'probability of forgetting an experience before an update')
cmd:option('--baseline', 0.5, 'baseline reward (in LAS score) to reduce variation')
cmd:option('--rootLabel', 'root', 'a string for root label, different when using LTH or Stanford dependencies')
cmd:text()
opt = cmd:parse(arg or {})
assert(opt.input_model ~= nil and opt.output_model ~= nil and
        opt.train_path ~= nil and opt.valid_path ~= nil)
root_dep_label = opt.rootLabel
print('Command line options: ')
print(opt)

local cfg = {
    learningRate = opt.learningRate,
    training_batch_size = opt.batchSize,
    training_subbatch_size = opt.subbatchSize,
    max_iters = opt.maxIters,
    criterion = NegativeScaleCriterion(),
    l2_weight = opt.l2,
    best_path = paths.tmpname(),
    reporting_frequency = 10,
    feature_templates = ChenManningFeatures(),
    include_punc = false, -- include punctuation in development-set evaluation
    sampling = opt.sampling,
    sample_size = opt.sampleSize,
    cuda = opt.cuda,
    baseline = opt.baseline,
    forget_prob = opt.forget_prob,
    max_iters_without_improvement = opt.max_iters_without_improvement,
}
print('General config: ')
print(cfg)

local vocabs, actions
if opt.cuda or opt.cuda_model then
    require('cunn') -- in case model was saved with cunn tensors
    if rand_seed then
        cutorch.manualSeed(rand_seed)
    end
end

if opt.input_model:find('%.txt$') or opt.input_model:find('%.txt%.gz$') then
    cfg.mlp, vocabs, actions = load_stanford_model(opt.input_model, 0)
else
    if opt.input_model:find('%.txt$') or opt.input_model:find('%.txt%.gz$') then
        cfg.mlp, vocabs, actions = load_stanford_model(opt.input_model)
    else
        cfg.mlp, vocabs, actions = table.unpack(torch.load(opt.input_model))
    end
end

if opt.cuda then
    cfg.mlp:cuda()
    cfg.criterion:cuda()
else
    cfg.mlp:float()
end
print('Model: ')
print(tostring(cfg.mlp))

function load_ds(path, name)
    if path:find('.th7$') then
        return torch.load(path)
    else
        local conll = CoNLL()
        conll.vocabs = vocabs
        local max_rows = iter_size(io.lines(path))+1
        return conll:build_dataset(path, name, max_rows)
    end
end

local train_ds = load_ds(opt.train_path, 'train')
-- for debugging
-- train_ds.sents = train_ds.sents:narrow(1, 1, 1000)
local valid_ds = load_ds(opt.valid_path, 'valid')

reinforce_parser(cfg, vocabs, actions, train_ds, valid_ds)

torch.save(opt.output_model, {cfg.best_mlp, vocabs, actions})
