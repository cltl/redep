require 'dep.config'
require 'dep.task'
require 'data'
require 'util'

--
-- Divide train, dev and test sets
--

local function iter_paths_in_sections(from, to, dir)
    return iter_chain2(iter_map(
            function(i) 
                return find_files_recursively(
                        paths.concat(dir, string.format("%02d", i))) 
            end, iter_range(from, to)))
end

local function erase_and_concat(inp_paths, out_path)
    os.execute('echo "" > ' .. out_path)
    for path in inp_paths do
        os.execute('cat ' .. path .. ' >> ' .. out_path)   
    end
end

local penn_dir = paths.concat(out_dir, 'penntree')
mkdirs(penn_dir)
local penn_train_path = paths.concat(penn_dir, 'train.mrg')
local penn_valid_path = paths.concat(penn_dir, 'valid.mrg')
local penn_test_path = paths.concat(penn_dir, 'test.mrg')
erase_and_concat(iter_paths_in_sections(2, 21, data_dir), penn_train_path)
erase_and_concat(iter_paths_in_sections(22, 22, data_dir), penn_valid_path)
erase_and_concat(iter_paths_in_sections(23, 23, data_dir), penn_test_path)


if jackknife_dir then
    --
    -- Measure POS tagging accuracy
    --
    
    local function write_iter_to_file(iter, path)
        local f = io.open(path, 'wt')
        for s in iter do
            f:write(s):write('\n')
        end
        f:close()
    end
    
    mkdirs(jackknife_dir)
    local jkf_train_path = paths.concat(jackknife_dir, 'train.mrg')
    local jkf_valid_path = paths.concat(jackknife_dir, 'valid.mrg')
    local jkf_test_path = paths.concat(jackknife_dir, 'test.mrg')
    
    local spt = 'stanford-postagger-2015-12-09'
    local class_path = spt .. '/stanford-postagger-3.6.0.jar:' .. spt .. '/lib/slf4j-simple.jar:' .. spt .. '/lib/slf4j-api.jar'
    local data_gen = function() return penn_sentences(penn_train_path) end
    os.execute('echo "" > ' .. jkf_train_path)
    for i, fold in ipairs(kfold_contiguous(data_gen, 10)) do
        local jkf_model_path = paths.concat(out_dir, string.format('jackknife-%02d.model', i)) 
        if jackknife_cache_models and paths.filep(jkf_model_path) then
            print('Model exists at ' .. jkf_model_path .. '. Aborted training.')
        else
            local model_train_path = paths.concat(out_dir, string.format('train_jackknife_%02d.mrg', i))
            write_iter_to_file(fold.train(), model_train_path)
            exec('java -mx2g -classpath ' .. class_path .. ' edu.stanford.nlp.tagger.maxent.MaxentTagger ' ..
                    '-props stanford-postagger-2015-12-09/penn-treebank.props ' ..
                    '-model ' .. jkf_model_path .. ' -trainFile format=TREES,' .. model_train_path)
        end
    
        local model_test_path = paths.concat(out_dir, string.format('test_jackknife_%02d.mrg', i))
        write_iter_to_file(fold.test(), model_test_path)
        local model_err_path = paths.concat(out_dir, string.format('jackknife-%02d.out', i))
        jackknife(model_test_path, jkf_train_path, model_err_path, jkf_model_path, true)
        print(string.format('Tagging and measuring accuracy fold %02d... Done.', i))
    end
    
    -- train POS tagger on train dataset and apply on development and test set
    local jkf_model_path = paths.concat(out_dir, 'jackknife-all.model')
    if jackknife_cache_models and paths.filep(jkf_model_path) then
        print('Model exists at ' .. jkf_model_path .. '. Aborted training.')
    else
        exec('java -mx2g -classpath ' .. class_path .. ' edu.stanford.nlp.tagger.maxent.MaxentTagger ' ..
                '-props stanford-postagger-2015-12-09/penn-treebank.props ' ..
                '-model ' .. jkf_model_path .. ' -trainFile format=TREES,' .. penn_train_path)
    end
    local err_valid_path = paths.concat(out_dir, string.format('jackknife-valid.err', i))
    local err_test_path = paths.concat(out_dir, string.format('jackknife-test.err', i))
    jackknife(penn_valid_path, jkf_valid_path, err_valid_path, jkf_model_path)
    jackknife(penn_test_path, jkf_test_path, err_test_path, jkf_model_path)
    
    print('Jackknifing POS tags... Done')
    
    -- replacing Penn sentences with jackknifed ones
    penn_dir = jackknife_dir
end

--
-- Convert to dependency using LTH converter
--
penn2dep_lth(penn_dir, lth_dir)

--
-- Convert to dependency using Stanford converter
--
penn2sd(penn_dir, sd_dir)

--
-- Compile general structures
--

local function save_info(corpus_dir, vocab_path, action_path)
    local conll = CoNLL(1, false, false)
    conll:prepare(find_files_recursively(corpus_dir), 'all', 2497203)
    torch.save(vocab_path, conll.vocabs)
    print('Input vocabulary written to ' .. vocab_path)
    
    local rels = {}
    for rel, _ in pairs(conll.vocabs.label.word2index) do
        table.insert(rels, rel)
    end
    local actions = make_actions(rels, conll.vocabs.label)
    torch.save(action_path, actions)
    print('Input vocabulary size: ' .. max_index(conll.vocabs))
    print('Action information written to ' .. action_path)
    print('Action vocabulary size: ' .. max_index(actions.vocab))
    return conll
end

-- 
-- Build standard datasets (Chen & Manning, 2014)
-- 

root_dep_label = 'root'
local conll = save_info(sd_dir, sd_vocab_path, sd_action_path)
torch.save(sd_train_path, conll:build_dataset(
        paths.concat(sd_dir, 'train.mrg.dep'), 'train', 3000000))
print('[sd] Training set written to ' .. sd_train_path)
torch.save(sd_valid_path, conll:build_dataset(
        paths.concat(sd_dir, 'valid.mrg.dep'), 'valid', 300000))
print('[sd] Development set written to ' .. sd_valid_path)
torch.save(sd_test_path, conll:build_dataset(
        paths.concat(sd_dir, 'test.mrg.dep'), 'test', 300000))
print('[sd] Development set written to ' .. sd_test_path)
-- os.execute('cp ' .. paths.concat(sd_dir, 'test.mrg.dep') .. ' ' .. sd_test_path)
-- print('[sd] Golden CoNLL dataset written to ' .. sd_test_path) 

root_dep_label = 'ROOT'
local conll = save_info(lth_dir, lth_vocab_path, lth_action_path)
torch.save(lth_train_path, conll:build_dataset(
        paths.concat(lth_dir, 'train.mrg.dep'), 'train', 3000000))
print('[lth] Training set written to ' .. lth_train_path)
torch.save(lth_valid_path, conll:build_dataset(
        paths.concat(lth_dir, 'valid.mrg.dep'), 'valid', 300000))
print('[lth] Development set written to ' .. lth_valid_path)
torch.save(lth_test_path, conll:build_dataset(
        paths.concat(lth_dir, 'test.mrg.dep'), 'test', 300000))
print('[lth] Development set written to ' .. lth_test_path)
-- os.execute('cp ' .. paths.concat(lth_dir, 'test.mrg.dep') .. ' ' .. lth_test_path)
-- print('[lth] Golden CoNLL dataset written to ' .. lth_test_path) 
