require('util')
require('data')
require('paths')
require('model')
require('dep.config')
require('math')
require('optim')

local function check_right_dependency(gold_links, pred_links, s1)
    if cget_right_dependent(gold_links, s1) == nil then
        return true;
    elseif cget_right_dependent(pred_links, s1) ~= nil then
        if cget_right_dependent(gold_links, s1) == cget_right_dependent(pred_links, s1) then
            return true;
        end
    end
    return false;
end

local _Parser = torch.class('Parser')

    function _Parser:reset()
        error('Not implemented')        
    end

    function _Parser:predict() 
    end
    
    function _Parser:execute(action) 
        error('Not implemented')        
    end


local function indices2mask(indices, mask)
    -- receive an array of integers or set of integers
    -- set elements corresponding to those integers to 1, leave others untouched 
    for _, v in ipairs(indices) do
        if type(v) == 'number' then
            mask[v] = 1
        elseif type(v) == 'table' then
            for i, _ in pairs(v) do
                mask[i] = 1
            end
        else
            error('Unsupported value: ' .. v)
        end
    end
end

local _ArcStandardTransitionSystem, parent = torch.class('ArcStandardTransitionSystem', 'Parser')

    function _ArcStandardTransitionSystem:__init(actions, tokens)
        self.actions = actions
        if tokens ~= nil then
            self:reset(tokens)
        end
    end

    _ArcStandardTransitionSystem.states = {
        terminal = 0,
        left_right_shift = 1,
        right_root_shift = 2,
        right_root = 3,
        left_right = 4,
        shift = 5,
    }

    function _ArcStandardTransitionSystem:build_masks()
        local actions = self.actions
        local states = _ArcStandardTransitionSystem.states
        local masks = torch.ByteTensor(5, max_index(actions.vocab))
        masks:zero()
        indices2mask({actions.left, actions.right, actions.shift}, masks[states.left_right_shift])
        indices2mask({actions.shift}, masks[states.shift])
        indices2mask({actions.left, actions.right}, masks[states.left_right])
        indices2mask({actions.right_root, actions.shift}, masks[states.right_root_shift])
        indices2mask({actions.right_root}, masks[states.right_root])
        return masks
    end

    function _ArcStandardTransitionSystem:reset(tokens)
        self.tokens = tokens
        self.last_action = nil
        self.stack = {}
        self.buffer = list(range(tokens:size(1)))
        self.links = torch.LongTensor(tokens:size(1), 5):zero()
        
        self:execute(self.actions.shift) 
        self:execute(self.actions.shift)
    end
    
    function _ArcStandardTransitionSystem:clone()
        local new_parser = ArcStandardTransitionSystem(self.actions)
        new_parser.tokens = self.tokens
        new_parser.last_action = shallow_copy(self.last_action)
        new_parser.stack = shallow_copy(self.stack)
        new_parser.buffer = shallow_copy(self.buffer)
        new_parser.links = self.links:clone()
        return new_parser
    end
    
    function _ArcStandardTransitionSystem:state()
        if #self.buffer >= 1 then
            if #self.stack > 2 then
                return self.states.left_right_shift
            else
                return self.states.shift
            end
        else
            if #self.stack > 2 then
                return self.states.left_right
            elseif #self.stack == 2 then
                assert(self.stack[1] ~= 1 and self.stack[2] == 1,
                        "Expecting root followed by something else in the stack")
                return self.states.right_root
            end
        end
        return self.states.terminal
    end
    
    function _ArcStandardTransitionSystem:execute(a)
        s1, s2 = self.stack[1], self.stack[2]
        if self.actions.left[a] then
            assert(#self.stack >= 2)
            self.links[{s2, 1}] = s1
            self.links[{s2, 2}] = self.actions.action2up[a] or self.actions.action2rel[a]
            self.links[{s2, 3}] = self.actions.action2down[a] or self.actions.action2rel[a]
            -- 4th column is leftmost child, 5th is siblings
            self.links[{s1, 4}] = ll_insert(self.links:select(2, 5), self.links[{s1, 4}], s2)
            table.remove(self.stack, 2)
        elseif self.actions.right[a] or a == self.actions.right_root then
            assert(#self.stack >= 2)
            self.links[{s1, 1}] = s2
            self.links[{s1, 2}] = self.actions.action2up[a] or self.actions.action2rel[a]
            self.links[{s1, 3}] = self.actions.action2down[a] or self.actions.action2rel[a]
            -- 4th column is leftmost child, 5th is siblings
            self.links[{s2, 4}] = ll_insert(self.links:select(2, 5), self.links[{s2, 4}], s1)
            table.remove(self.stack, 1)
        elseif a == self.actions.shift then
            assert(#self.buffer >= 1)
            table.insert(self.stack, 1, table.remove(self.buffer, 1))
        else
            error('Unsupported action: ' .. tostring(a))
        end
        self.last_action = {
            value = a, prev = self.last_action
        }
    end
    
    function _ArcStandardTransitionSystem:get_executed_actions()
        local tbl = {}
        local curr = self.last_action
        while curr do
            table.insert(tbl, curr.value)
            curr = curr.prev
        end
        for i = 1, #tbl/2 do
            tbl[i], tbl[#tbl-i+1] = tbl[#tbl-i+1], tbl[i]
        end
        return tbl
    end

local _ShortestStackArcStandardOracle = torch.class('ShortestStackArcStandardOracle')

    function _ShortestStackArcStandardOracle:__init(input_vocabs, actions, tokens)
        -- tokens is optional
        self.input_vocabs = input_vocabs
        self.parser = ArcStandardTransitionSystem(actions, tokens)
    end

    function _ShortestStackArcStandardOracle:next()
        --[[
            based on Malt parser's implementation:
            http://grepcode.com/file/repo1.maven.org/maven2/org.maltparser/maltparser/1.8/org/maltparser/parser/algorithm/nivre/ArcStandardOracle.java#ArcStandardOracle
        --]]
        local debug = false
        local tokens = self.parser.tokens
        local actions = self.parser.actions
        local buffer = self.parser.buffer
        local stack = self.parser.stack
        local gold_links = tokens:narrow(2, 3, 3)
        if #stack < 2 then
            if #buffer <= 0 then
                assert(stack[1] == 1)
                return nil
            else
                if debug then print('shift') end
                return actions.shift
            end
        else
            s1, s2 = stack[1], stack[2]
            if tokens[{s2, 3}] == s1 then
                if debug then print('left') end
                return actions.rel2left[tokens[{s2, 4}]]
            elseif tokens[{s1, 3}] == s2 and 
                    check_right_dependency(gold_links, self.parser.links, s1) then
                if debug then print('right') end
                return actions.rel2right[tokens[{s1, 4}]]
            elseif #buffer >= 1 then
                if debug then print('shift') end
                return actions.shift
            else
                error("You shouldn't see this! Probably unprojective tree was used.")
            end
        end
    end

    function _ShortestStackArcStandardOracle:run_until_terminated()
        while self.parser:state() ~= self.parser.states.terminal do
            local action = self:next()
            if action then
                self.parser:execute(action)
            end
        end
        return self
    end

    function _ShortestStackArcStandardOracle:build_dataset(ds, feature_extractor, name, max_rows) 
        name = name or '[noname]'
        print(string.format("Running oracle on dataset '%s'... ", name))
        start = os.time()
        local x = torch.LongTensor(max_rows, feature_extractor:num())
        local states = torch.LongTensor(max_rows)
        local y = torch.LongTensor(max_rows)
        x:fill(2) -- __NONE__
        local count = 0
        for s = 1, ds.sents:size(1) do
            local tokens = ds.tokens:narrow(1, ds.sents[{s, 2}], ds.sents[{s, 3}])
            if is_projective(tokens:select(2, 3)) then
                assert(tokens[{1, 3}] == 0) -- first token must be root
                self.parser:reset(tokens)
                -- print(table.concat(self.parser.stack, ','), '\t', table.concat(self.parser.buffer, ','))
                local action = self:next()
                while action ~= nil do
                    count = count + 1
                    feature_extractor(x[count], tokens, self.parser.links, 
                            self.parser.stack, self.parser.buffer, self.input_vocabs)
                    states[count] = self.parser:state()
                    y[count] = action
                    self.parser:execute(action)
                    -- print(table.concat(self.parser.stack, ','), '\t', table.concat(self.parser.buffer, ','))
                    action = self:next()
                    if count % 100000 == 0 then
                        print(count .. ' examples...')
                    end
                end
            end
        end
        x = x:narrow(1, 1, count)
        states = states:narrow(1, 1, count)
        y = y:narrow(1, 1, count)
        collectgarbage() -- important! avoid memory error
        stop = os.time()
        print(string.format("Running oracle on dataset '%s'... Done (%d examples, %d s).", 
                name, y:size(1), stop-start))
        return {x, states}, y
    end
    
    
local _BatchGreedyArcStandardParser = torch.class('BatchGreedyArcStandardParser')

    function _BatchGreedyArcStandardParser:__init(input_vocabs, actions, 
            feature_extractor, mlp, cuda, report_frequency)
        -- the mlp must be masked, so that it returns log probability
        -- for valid actions and -inf for the rest
        self.input_vocabs = input_vocabs
        self.actions = actions
        self.feature_extractor = feature_extractor
        self.mlp = mlp
        self.cuda = cuda
        self.report_frequency = report_frequency or 1000
    end

    function _BatchGreedyArcStandardParser:predict(ds, batch_size, output, parse_scores)
        local s = 1
        local actual_batch_size = math.min(batch_size, ds.sents:size(1))
        -- print('Actual batch size: ' .. tostring(actual_batch_size))
        local x = torch.LongTensor(actual_batch_size, self.feature_extractor:num())
        local states = torch.LongTensor(actual_batch_size)
        x:fill(1) -- avoid "index out of range"
        states:fill(1) -- avoid "index out of range"
        local max_scores, y -- to be set by mlp
        local start_time = os.time()
        local finished_sent_count = 0
        output = output or torch.LongTensor(ds.tokens:size(1), 3):zero()
        parse_scores = parse_scores or torch.Tensor(ds.sents:size(1)):zero()

        local single_parse = function(index)
            local parser = ArcStandardTransitionSystem(self.actions)
            while s <= ds.sents:size(1) do
                local local_s = s
                s = s + 1
                local start = ds.sents[{local_s, 2}]
                local size = ds.sents[{local_s, 3}]
                local tokens = ds.tokens:narrow(1, start, size) 
                parser:reset(tokens)
                while parser:state() ~= parser.states.terminal do
                    states[index] = parser:state()
                    self.feature_extractor(x[index], tokens, parser.links, 
                            parser.stack, parser.buffer, self.input_vocabs)
                    coroutine.yield() -- wait for result
                    -- action_scores should be filled with results after this point
                    parse_scores[local_s] = parse_scores[local_s] + max_scores[index][1]
                    parser:execute(y[index][1])
                end
                -- any error will be checked on coroutine.resume
                assert(#parser.stack == 1 and #parser.buffer == 0 
                        and parser.stack[1] == 1, 'Unfinished parse.')
                output:narrow(1, start, size):copy(parser.links:narrow(2, 1, 3))
                
                finished_sent_count = finished_sent_count + 1
                if self.report_frequency > 0 and finished_sent_count % self.report_frequency == 0 then
                    print(string.format("Sentence %d, speed = %.2f sentences/s", 
                            finished_sent_count, finished_sent_count/(os.time()-start_time)))
                end
            end
        end
        
        local workers = coroutine_create_many(single_parse, actual_batch_size)
        while coroutine_resume_all(workers) do
            if self.cuda then 
                max_scores, y = self.mlp:forward({x:cuda(), states:cuda()}):max(2)
                max_scores = max_scores:float()
                y = y:long()
            else
                max_scores, y = self.mlp:forward({x, states}):max(2)
            end
        end
        return output, parse_scores
    end
    
    
local _Beam = torch.class('Beam')     

    function _Beam:__init(size, prob, max_size)
        assert((size ~= nil and size > 0) ~= (prob ~= nil and prob > 0))
        self.parsers = {}
        self.scores = {}
        self.size = size
        self.prob = prob
        self.max_size = max_size
    end

    function _Beam:register(score, parser, action_to_execute)
        if tostring(score):find('inf') then return false end
        
        local min_score = math.huge
        local min_index = 0
        local actual_prob = 0
        for i = 1, #self.parsers do
            actual_prob = actual_prob + math.exp(self.scores[i])
            min_score = self.scores[i]
            min_index = i
        end
        
        local index = 0
        if (self.size and #self.parsers < self.size) or
                (self.prob and actual_prob < self.prob and #self.parsers < self.max_size) then
            index = #self.parsers + 1
        elseif score > min_score then
            index = min_index
        end
        if index > 0 then
            if action_to_execute then
                parser = parser:clone()
                parser:execute(action_to_execute)
            end
            self.parsers[index] = parser
            self.scores[index] = score
            return true
        else
            return false
        end
    end
    
    function _Beam:new_instance()
        return Beam(self.size, self.prob, self.max_size)
    end

local _BatchBeamArcStandardParser = torch.class('BatchBeamArcStandardParser')

    function _BatchBeamArcStandardParser:__init(input_vocabs, actions, 
            feature_extractor, mlp, beam_size, cuda, report_frequency, beam_callback)
        -- the mlp must be masked, so that it returns log probability
        -- for valid actions and -inf for the rest
        self.input_vocabs = input_vocabs
        self.actions = actions
        self.feature_extractor = feature_extractor
        self.mlp = mlp
        self.beam_size = beam_size
        self.cuda = cuda
        self.report_frequency = report_frequency or 1000
        self.beam_callback = beam_callback
    end

    function _BatchBeamArcStandardParser:predict(ds, batch_size, output, parse_scores)
        local s = 1
        local x = torch.LongTensor(batch_size, self.feature_extractor:num())
        local states = torch.LongTensor(batch_size)
        x:fill(1) -- avoid "index out of range"
        states:fill(1) -- avoid "index out of range"
        local action_scores = torch.Tensor() -- to be set by mlp
        output = output or torch.LongTensor(ds.tokens:size(1), 3):zero()
        parse_scores = parse_scores or torch.Tensor(ds.sents:size(1)):zero()
        local start_time -- will be initialized later
        local finished_sent_count = 0

        local beam_parse = function(index)
            while s <= ds.sents:size(1) do
                local local_s = s
                s = s + 1

                local start = ds.sents[{local_s, 2}]
                local size = ds.sents[{local_s, 3}]
                local tokens = ds.tokens:narrow(1, start, size) 
                local beam = Beam(self.beam_size)
                beam:register(0, ArcStandardTransitionSystem(self.actions, tokens))                
                while true do
                    local any_active = false
                    for i, parser in ipairs(beam.parsers) do
                        if parser:state() ~= parser.states.terminal then
                            states[(index-1)*self.beam_size + i] = parser:state()
                            self.feature_extractor(x[(index-1)*self.beam_size + i], tokens, parser.links, 
                                    parser.stack, parser.buffer, self.input_vocabs)
                            any_active = true
                        end
                    end
                    if not any_active then break end
                    coroutine.yield() -- wait for result
                    -- action_scores should be filled with results after this point
                    local new_beam = beam:new_instance()
                    for i, parser in ipairs(beam.parsers) do
                        if parser:state() == parser.states.terminal then
                            -- any error will be checked on coroutine.resume
                            assert(#parser.stack == 1 and #parser.buffer == 0 
                                    and parser.stack[1] == 1, 'Unfinished parse.')
                            new_beam:register(beam.scores[i], parser)
                        else
                            local sorted_scores, sorted_actions = 
                                    action_scores[(index-1)*self.beam_size + i]:sort(true)
                            for j = 1, sorted_scores:size(1) do
                                local registered = new_beam:register(
                                        beam.scores[i] + sorted_scores[j],
                                        parser, sorted_actions[j])
                                if not registered then break end
                            end
                        end
                    end
                    beam = new_beam
                end
                
                local max_score, max_index = list_max(beam.scores)
                local parser = beam.parsers[max_index]
                output:narrow(1, start, size):copy(parser.links:narrow(2, 1, 3))
                parse_scores[local_s] = beam.scores[max_index]

                if self.beam_callback then
                    self.beam_callback(beam)
                end
                finished_sent_count = finished_sent_count + 1
                if self.report_frequency > 0 and finished_sent_count % self.report_frequency == 0 then
                    print(string.format("Sentence %d, speed = %.2f sentences/s", 
                            finished_sent_count, finished_sent_count/(os.time()-start_time)))
                end
            end
        end
        
        local batch_masks = torch.ByteTensor()
        local workers = {}
        for i = 1, math.min(batch_size, ds.sents:size(1))/self.beam_size do
            workers[i] = coroutine.create(function() beam_parse(i) end) 
        end
        start_time = os.time()
        while coroutine_resume_all(workers) do
            -- print('<<< ' .. self.idx)
            if self.cuda then 
                action_scores = self.mlp:forward({x:cuda(), states:cuda()}):float()
            else
                action_scores = self.mlp:forward({x, states})
            end
            -- print('>>> ' .. self.idx)
        end
--        print("Finished: " .. self.idx)
        return output, parse_scores
    end
    

local _MultiThreadParser = torch.class('MultiThreadParser')

    function _MultiThreadParser:__init(parser, num_threads)
        print('[WARN] This multi-thread parser is not stable. Errors ahead.')
        self.parser = parser
        self.num_threads = num_threads
    end
    
    function _MultiThreadParser:predict(ds, ...)
        local params = {...}
        local output = torch.LongTensor(ds.tokens:size(1), 3):zero()
        local parse_scores = torch.Tensor(ds.sents:size(1)):zero()
        table.insert(params, output)
        table.insert(params, parse_scores)
        local Threads = require('threads')
        Threads.serialization('threads.sharedserialize')
        local threads = Threads(
            self.num_threads,
            function()
                require('cunn')
                require('dep.task')
                require('math')
            end,
            function()
                self.parser.mlp = self.parser.mlp:clone('weight', 'bias')
            end
        )
        for i = 1, self.num_threads do 
            threads:addjob(
                function(idx)
                   local start = math.floor((idx-1) * ds.sents:size(1)/self.num_threads) + 1
                   local stop = math.floor(idx * ds.sents:size(1)/self.num_threads)
                   local ds_part = {
                        sents = ds.sents:narrow(1, start, stop-start+1), 
                        tokens = ds.tokens
                   }
                   self.parser.idx = idx
                   self.parser:predict(ds_part, table.unpack(params))
                end,
                nil, -- end callback
                i
            )
        end
        threads:synchronize()
        threads:terminate()
        return output, parse_scores
    end    
    
function parse_and_measure_uas(ds, parser, batch_size, include_punc)
    batch_size = batch_size or 512
    local output = parser:predict(ds, batch_size)
    local mask = ds.tokens:select(2, 3):ne(0) -- not root
    if not include_punc then
        -- match Stanford implementation here:
        -- https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/parser/nndep/ParsingSystem.java#L130
        for _, p in ipairs{"``", "''", ".", ",", ":"} do
            local index = parser.input_vocabs.pos:get_index(p)
            mask:cmul(ds.tokens:select(2, 2):ne(index))
        end
    end
    local gold_heads = ds.tokens:select(2, 3)[mask]
    local pred_heads = output:select(2, 1)[mask]
    local correct = pred_heads:eq(gold_heads):sum()
    return correct / gold_heads:size(1)
end

function measure_las(gold_tokens, pred_links, include_punc, input_vocabs)
    local mask = gold_tokens:select(2, 3):ne(0) -- not root
    if not include_punc then
        -- match Stanford implementation here:
        -- https://github.com/stanfordnlp/CoreNLP/blob/master/src/edu/stanford/nlp/parser/nndep/ParsingSystem.java#L130
        for _, p in ipairs{"``", "''", ".", ",", ":"} do
            local index = input_vocabs.pos:get_index(p)
            mask:cmul(gold_tokens:select(2, 2):ne(index))
        end
    end
    local gold_heads = gold_tokens:select(2, 3)[mask]
    local gold_labels = gold_tokens:select(2, 4)[mask]
    local pred_heads = pred_links:select(2, 1)[mask]
    local pred_labels = pred_links:select(2, 2)[mask]
    local correct = pred_heads:eq(gold_heads):cmul(pred_labels:eq(gold_labels)):sum()
    return correct / gold_heads:size(1)
end

function parse_and_measure_las(ds, parser, batch_size, include_punc)
    batch_size = batch_size or 512
    local output = parser:predict(ds, batch_size)
    local mask = ds.tokens:select(2, 3):ne(0) -- not root
    if not include_punc then
        -- use the set of punctuation from Chen & Manning (2014)
        for _, p in ipairs{"``", "''", ".", ",", ":"} do
            local index = parser.input_vocabs.pos:get_index(p)
            mask:cmul(ds.tokens:select(2, 2):ne(index))
        end
    end
    local gold_heads = ds.tokens:select(2, 3)[mask]
    local gold_labels = ds.tokens:select(2, 4)[mask]
    local pred_heads = output:select(2, 1)[mask]
    local pred_labels = output:select(2, 2)[mask]
    local correct = pred_heads:eq(gold_heads):cmul(pred_labels:eq(gold_labels)):sum()
    return correct / gold_heads:size(1)
end

function train_parser(cfg, parser, train_x, train_y, valid_ds)
    print(string.format("Training %s...", cfg.model_name))
    local start = os.time()
    local example_count = 0
    local batch_count = math.max(input_size(train_x)/cfg.training_batch_size, 1)
    local best_score = -math.huge
    local params, grad_params = cfg.mlp:getParameters()
    local batch_x, batch_y -- sampled x and y (avoid allocating multiple times)
    local indices = torch.LongTensor() -- sampled indices
    local optim_state = {
            learningRate = cfg.learningRate, 
            learningRateDecay = cfg.learningRateDecay,
            weightDecay = 0, 
            ada_eps = cfg.ada_eps,
    }
    assert(optim_state.learningRate > 0)
    assert(optim_state.learningRateDecay >= 0)
    assert(optim_state.ada_eps > 0)
    for iter_count = 1, cfg.max_iters do
        io.write(string.format('Epoch %d... ', iter_count))
        batch_x, batch_y = random_unique_n(train_x, train_y, batch_x, batch_y, 
                                           indices, cfg.training_batch_size)
        cfg.mlp:training() -- affects dropout layer, if present

        local cost = cfg.criterion:forward(cfg.mlp:forward(batch_x), batch_y)
        assert(tostring(cost) ~= 'nan' and not tostring(cost):find('inf'))
        grad_params:zero()
        cfg.mlp:backward(batch_x, cfg.criterion:backward(cfg.mlp.output, batch_y))
        if cfg.l2_weight and cfg.l2_weight > 0 then
            grad_params:add(cfg.l2_weight, params)
        end
        custom_adagrad(function() return nil, grad_params end, params, optim_state)
        -- optim.adadelta(function() return nil, grad_params end, params, optim_state)
        
        example_count = example_count + cfg.training_batch_size
        local speed = example_count / (os.time()-start)
        io.write(string.format("Done (cost: %.2f, %d s, %.2fk examples/s).\n", 
                cost, os.time()-start, speed/1000))
        
        if iter_count % 100 == 0 then
            cfg.mlp:evaluate() -- affects dropout layer, if present
            local score = parse_and_measure_uas(valid_ds, parser, 
                    cfg.monitoring_batch_size, cfg.include_punc)
            io.write(string.format('UAS on development set: %.3f%%\n', score*100))
            if score > best_score then
                io.write(string.format('Writing best model to %s... ', cfg.best_path)) 
                local light_mlp = cfg.mlp:clone('weight','bias')
                torch.save(cfg.best_path, light_mlp)
                best_score = score
                io.write('Done.\n')
            end
        end
        
        if iter_count % 100 == 0 then
            local speed = example_count / (os.time()-start)
            io.write(string.format('Iteration %d finished (%.2f mins, %.2fk examples/s).\n', 
                    iter_count, (os.time()-start)/60, speed/1000))
        end
    end
    
    io.write(string.format('Loading from %s... ', cfg.best_path))
    cfg.best_mlp = torch.load(cfg.best_path)
    io.write('Done.\n')

    io.write(string.format("Training %s... Done (%.2f min).\n", cfg.model_name, (os.time()-start)/60.0))
end

local function pad_with_slash(s)
    if not s:endsWith('/') then
        return s .. '/'
    else
        return s
    end
end
    
function penn2dep_lth(inp_dir_or_file, out_dir_or_file)
    if paths.filep(inp_dir_or_file) then
        mkdirs(paths.dirname(out_dir_or_file))
        exec('java -jar pennconverter/pennconverter.jar ' ..
                '-stopOnError=False -raw < %s > %s.dep', inp_dir_or_file, out_dir_or_file)
    else
        inp_dir_or_file = pad_with_slash(inp_dir_or_file)
        out_dir_or_file = pad_with_slash(out_dir_or_file) 
        for inp_path in find_files_recursively(inp_dir_or_file) do
            local out_path = inp_path:gsub(inp_dir_or_file:quote(), out_dir_or_file)
            penn2dep_lth(inp_path, out_path)
        end
    end
end
    
function penn2sd(inp_dir_or_file, out_dir_or_file)
    if paths.filep(inp_dir_or_file) then
        mkdirs(paths.dirname(out_dir_or_file))
        exec('java -cp stanford-parser-full-2013-11-12/stanford-parser.jar ' ..
                'edu.stanford.nlp.trees.EnglishGrammaticalStructure -basic -conllx ' ..
                '-treeFile ' .. inp_dir_or_file .. ' > ' .. out_dir_or_file .. '.dep')
    else
        inp_dir_or_file = pad_with_slash(inp_dir_or_file)
        out_dir_or_file = pad_with_slash(out_dir_or_file) 
        for inp_path in find_files_recursively(inp_dir_or_file) do
            local out_path = inp_path:gsub(inp_dir_or_file:quote(), out_dir_or_file)
            penn2sd(inp_path, out_path)
        end
    end
end

function jackknife(path, jkf_path, err_path, model_path, append)
    local changed = 0
    local total = 0
    mkdirs(paths.dirname(jkf_path))
    local tmp = paths.tmpname()
    local spt = 'stanford-postagger-2015-12-09'
    local class_path = spt .. '/stanford-postagger-3.6.0.jar:' .. spt .. '/lib/slf4j-simple.jar:' .. spt .. '/lib/slf4j-api.jar'
    exec('java -classpath ' .. class_path .. ' edu.stanford.nlp.tagger.maxent.MaxentTagger ' ..
            '-props stanford-postagger-2015-12-09/penn-treebank.props ' ..
            '-model ' .. model_path .. ' -testFile format=TREES,' .. path ..
            ' > ' .. tmp .. ' 2> ' .. err_path) 
    local f = io.open(jkf_path, ternary(append, 'a', 'w'))
    for tree_line, tmp_line in iter_zip(penn_sentences(path), io.lines(tmp)) do
        local word_pos_iter = list_iter(split(tmp_line, ' ')) 
        tree_line = tree_line:gsub('%((%S+) (%S+)%)', 
                function(tree_pos, tree_word) 
                    if tree_pos == '-NONE-' then
                        -- keep original POS tag
                        return string.format('(%s %s)', tree_pos, tree_word)
                    else
                        -- replace by predicted POS tag
                        local tmp_word, tmp_pos = table.unpack(split(word_pos_iter(), '_'))
                        if tree_word:gsub('\\/', '/'):gsub('\\%*', '*') ~= tmp_word and 
                                tree_word ~= 'theatre' and tree_word ~= 'august' and
                                tree_word ~= 'neighbours' then
                            print(string.format('WARN: Differing original and ' .. 
                                    'POS tagged words: %s vs. %s', tree_word, tmp_word))
                        end
                        if tree_pos ~= tmp_pos then 
                            changed = changed + 1 
                        end
                        total = total + 1
                        return string.format('(%s %s)', tmp_pos, tree_word)
                    end
                end)
        assert(word_pos_iter() == nil)
        f:write(tree_line):write('\n')
    end
    f:close()
    print(string.format('Changed: %d, total: %d, wrote to: %s', changed, total, jkf_path))
    os.execute('grep "Total tags right:" ' .. err_path)
    os.remove(tmp)
end

function make_actions(rels, label_vocab, directed_labels)
    local actions = {
        vocab = Vocabulary(),
        left = {},          -- set of all left actions (except root)
        right = {},         -- set of all right actions (except root) 
        rel2left = {},      -- map from relation (label) to left actions (except root) 
        rel2right = {},     -- map from relation (label) to right actions (except root) 
        action2rel = {},    -- map from actions (including root) to undirected relation (label)
        action2up = {},     -- map from actions (including root) to upward relation (label)
        action2down = {},   -- map from actions (including root) to downward relation (label)
    }
    -- keep the same order as makeTransitions in Stanford's parser 
    for _, rel in ipairs(rels) do
        if rel ~= '__NONE__' then
            actions.vocab:get_index('L(' .. rel .. ')')
        end
    end
    for _, rel in ipairs(rels) do
        if rel ~= '__NONE__' then
            actions.vocab:get_index('R(' .. rel .. ')')
        end
    end
    actions.vocab:get_index('S') -- remember shift action
    -- build some structures
    actions.right_root = actions.vocab.word2index['R(' .. root_dep_label .. ')']
    assert(actions.right_root, 'Wrong root label, maybe?') 
    for _, rel in ipairs(rels) do
        if rel ~= '__NONE__' then
            local l = actions.vocab:get_index('L(' .. rel .. ')')
            local r = actions.vocab:get_index('R(' .. rel .. ')')
            if directed_labels then
                local u = label_vocab:get_index(rel .. '#U')
                local d = label_vocab:get_index(rel .. '#D')
                if u ~= 1 then
                    if rel ~= root_dep_label then
                        actions.rel2left[u] = l 
                        actions.rel2right[u] = r
                    end
                    actions.action2up[l] = u
                    actions.action2up[r] = u
                end
                if d ~= 1 then 
                    if rel ~= root_dep_label then
                        actions.rel2left[d] = l 
                        actions.rel2right[d] = r
                    end
                    actions.action2down[l] = d
                    actions.action2down[r] = d
                end
            else
                local rel = label_vocab:get_index(rel)
                if rel ~= root_dep_label then
                    actions.rel2left[rel] = l 
                    actions.rel2right[rel] = r
                end
                actions.action2rel[l] = rel
                actions.action2rel[r] = rel
            end
            if rel ~= root_dep_label then
                actions.left[l] = true
                actions.right[r] = true
            end
        end
    end
    actions.shift = actions.vocab:get_index('S')
    return actions
end

function translate_magic_words(s)
    s = s:gsub('%-NULL%-', '__NONE__'):gsub('%-UNKNOWN%-', '__MISSING__'):gsub('%-ROOT%-', '__ROOT__')
    return s
end

function load_stanford_model(path, drop_prob)
    if path:find('%.gz$') then
        local unzipped = paths.tmpname()
        assert(os.execute(string.format('gunzip %s -c > %s', path, unzipped)))
        path = unzipped
    end
    local start = os.time()
    io.write("Loading depparse model file: " .. path .. "... ")
    local f = io.open(path) 
    local s = f:read('*line')
    local nDict = tonumber(s:sub(s:find('=') + 1))
    s = f:read('*line')
    local nPOS = tonumber(s:sub(s:find('=') + 1))
    s = f:read('*line')
    local nLabel = tonumber(s:sub(s:find('=') + 1))
    s = f:read('*line')
    local eSize = tonumber(s:sub(s:find('=') + 1))
    s = f:read('*line')
    local hSize = tonumber(s:sub(s:find('=') + 1))
    s = f:read('*line')
    local nTokens = tonumber(s:sub(s:find('=') + 1))
    s = f:read('*line')
    local nPreComputed = tonumber(s:sub(s:find('=') + 1))
    local mlp = new_cubenet(nDict+nPOS+nLabel, eSize, nTokens, hSize, 
            nLabel * 2 - 1, false, drop_prob)

    local indexer = Indexer()
    local vocabs = {
        word = Vocabulary(indexer),
        pos = Vocabulary(indexer),
        label = Vocabulary(indexer),
    }
    local knownLabels = {}
    local E = mlp:get_lookup_table().weight
    local splits

    for k = 1, nDict do
        local s = f:read('*line')
        local splits = split(s, " ");
        local index = vocabs.word:get_index(translate_magic_words(splits[1]))
        assert(#splits-1 == eSize)
        for i = 1, eSize do
            E[index][i] = tonumber(splits[i + 1]);
        end
    end
    for k = 1, nPOS do
        local s = f:read('*line')
        local splits = split(s, " ");
        local index = vocabs.pos:get_index(translate_magic_words(splits[1]))
        assert(#splits-1 == eSize)
        for i = 1, eSize do
            E[index][i] = tonumber(splits[i + 1]);
        end
    end
    for k = 1, nLabel do
        local s = f:read('*line')
        local splits = split(s, " ");
        local label = translate_magic_words(splits[1])
        local index = vocabs.label:get_index(label);
        table.insert(knownLabels, label)
        assert(#splits-1 == eSize)
        for i = 1, eSize do
            E[index][i] = tonumber(splits[i + 1]);
        end
    end
    assert(max_index(vocabs) == E:size(1))
    
    local W1 = mlp:get_hidden_layers()[1].weight
    for j = 1, W1:size(2) do
        local s = f:read('*line')
        local splits = split(s, " ");
        assert(#splits == W1:size(1))
        for i = 1, W1:size(1) do
            W1[i][j] = tonumber(splits[i]);
        end
    end

    local b1 = mlp:get_hidden_layers()[1].bias
    local s = f:read('*line')
    local splits = split(s, " ");
    assert(#splits == b1:size(1))
    for i = 1, b1:size(1) do
        b1[i] = tonumber(splits[i]);
    end

    local W2 = mlp:get_output_layer().weight
    for j = 1, W2:size(2) do
        local s = f:read('*line')
        local splits = split(s, " ");
        assert(#splits == W2:size(1))
        for i = 1, W2:size(1) do
            W2[i][j] = tonumber(splits[i]);
        end
    end
    -- skip pre-computed weights
    f:close()

    -- equivalent to makeTransitions() 
    local actions = make_actions(knownLabels, vocabs.label)
    -- print(vocabs.label)
    for _, vocab in pairs(vocabs) do
        vocab:seal(true)
    end
    mlp = new_masked_net(mlp, ArcStandardTransitionSystem(actions):build_masks(), true)
    
    print(string.format('Done (%s s).', os.time()-start))
    return mlp, vocabs, actions
end


local _DynamicArcStandardOracle = torch.class('DynamicArcStandardOracle')
    -- see Goldberg et al. (2014)             
    -- Goldberg, Y., Sartorio, F., & Satta, G. (2014). A tabular method for dynamic 
    -- oracles in transition-based parsing. Transactions of the Association for 
    -- Computational Linguistics, 2, 119–130.

    function _DynamicArcStandardOracle:make_right_stack(stack, buffer, gold_links)
        local right_stack = {}
        local is_in_left_stack = list2set(stack)
        local is_in_right_stack = {}
        local min_buffer = buffer[1]
        local max_buffer = buffer[#buffer]
        for _, i in ipairs(buffer) do
            local parent_i = gold_links[{i, 1}] 
            local added = not (parent_i >= min_buffer and parent_i <= max_buffer)
            if not added then
                for j = 1, gold_links:size(1) do
                    if gold_links[{j, 1}] == i and 
                            (is_in_left_stack[j] or is_in_right_stack[j]) then
                        added = true
                        break
                    end 
                end
            end 
            if added then
                table.insert(right_stack, i)
                is_in_right_stack[i] = true
            end        
        end
        return right_stack
    end

    function _DynamicArcStandardOracle:tree_frag_unlabeled_loss(root, links, gold_links)
        local loss = 0 
        for i = 1, links:size(1) do
            if links[{i, 1}] == root then
                if links[{i, 1}] ~= gold_links[{i, 1}] then
                    loss = loss + 1
                end
                loss = loss + self:tree_frag_unlabeled_loss(i, links, gold_links)
            end
        end
        return loss
    end

    function _DynamicArcStandardOracle:tree_frag_labeled_loss(root, links, gold_links)
        local loss = 0 
        for i = 1, links:size(1) do
            if links[{i, 1}] == root then
                if links[i]:ne(gold_links[i]):any() then
                    loss = loss + 1
                end
                loss = loss + self:tree_frag_labeled_loss(i, links, gold_links)
            end
        end
        return loss
    end
    
    function _DynamicArcStandardOracle:edge_delta(head, dependent, gold_links)
        if gold_links[{dependent, 1}] == head then
            return 0
        else
            return 1
        end 
    end
    
    function _DynamicArcStandardOracle:config_propagated_loss(sys, gold_links)
        local right_stack = self:make_right_stack(sys.stack, sys.buffer, gold_links)
        -- print(sys.stack, sys.buffer, sys.links, gold_links, right_stack)
        table.insert(right_stack, 1, sys.stack[1]) -- see the beginning of section 4.3
        -- starts implementing Algorithm 1
        local t = defaultdict(function ()  
                return defaultdict(function () 
                        return defaultdict(function() return math.huge end) 
                    end) 
            end)
        -- this line is according to the original algorithm but it seems incorrect
        -- because it cause Loss(t(stack[1]), t_G) to be added twice
        -- t[1][1][sys.stack[1]] = self:tree_frag_unlabeled_loss(sys.stack[1], sys.links, gold_links)
        t[1][1][sys.stack[1]] = 0
        local ll = #sys.stack
        local lr = #right_stack
        for d = 1, ll + lr - 1 do
            for j = math.max(1, d - ll + 1), math.min(d, lr) do -- column index
                local i = d - j + 1 -- row index
                local iter_Delta = function()
                    return iter_chain(iter_first_n(list_iter(sys.stack), i), 
                            iter_first_n(list_iter(right_stack), j))
                end
                if i < ll then
                    for h in iter_Delta() do
                        t[i+1][j][h] = math.min(t[i+1][j][h], 
                                t[i][j][h] + self:edge_delta(h, sys.stack[i+1], gold_links))
                        t[i+1][j][sys.stack[i+1]] = math.min(t[i+1][j][sys.stack[i+1]],
                                t[i][j][h] + self:edge_delta(sys.stack[i+1], h, gold_links))
                    end
                end
                if j < lr then
                    for h in iter_Delta() do
                        t[i][j+1][h] = math.min(t[i][j+1][h], 
                                t[i][j][h] + self:edge_delta(h, right_stack[j+1], gold_links))
                        t[i][j+1][right_stack[j+1]] = math.min(t[i][j+1][right_stack[j+1]], 
                                t[i][j][h] + self:edge_delta(right_stack[j+1], h, gold_links))
                    end
                end
            end
        end
        return t[ll][lr][1]
    end
    
    function _DynamicArcStandardOracle:config_unlabeled_loss(sys, gold_links)
        local loss = self:config_propagated_loss(sys, gold_links)
        local ll = #sys.stack
        for i = 1, ll do
            loss = loss + self:tree_frag_unlabeled_loss(sys.stack[i], 
                    sys.links:narrow(2, 1, 3), gold_links)
        end
        return loss
    end
    
    function _DynamicArcStandardOracle:config_labeled_loss(sys, gold_links)
        local loss = self:config_propagated_loss(sys, gold_links)
        local ll = #sys.stack
        for i = 1, ll do
            loss = loss + self:tree_frag_labeled_loss(sys.stack[i], 
                    sys.links:narrow(2, 1, 3), gold_links)
        end
        return loss
    end

    function _DynamicArcStandardOracle:next(sys, tokens)
        error('Not implemented yet')
    end
